package com.codegym.task.task13.task1326;

/* 
Sorting even numbers from a file

*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String fileName = reader.readLine();
        reader.close();

        List<Integer> list = new ArrayList<>();

        FileInputStream inStream = new FileInputStream(fileName);
        BufferedReader fileReader = new BufferedReader(new InputStreamReader(inStream));

        while (fileReader.ready()){
            int number = Integer.parseInt(fileReader.readLine());
            if (number % 2 == 0) {
                list.add(number);
            }
        }
        fileReader.close();

        Collections.sort(list);

        for (Integer element : list) {
            System.out.println(element);
        }
    }
}
