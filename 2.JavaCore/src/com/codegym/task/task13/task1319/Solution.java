package com.codegym.task.task13.task1319;

import java.io.*;

/* 
Writing to a file from the console

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter writer = new BufferedWriter(new FileWriter(reader.readLine()));

        String input = "";
        while (!input.equals("exit")) {
            input = reader.readLine();
            writer.write(input);
            writer.newLine();
        }
        writer.flush();

        reader.close();
        writer.close();
    }
}
