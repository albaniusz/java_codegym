package com.codegym.task.task13.task1318;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* 
Reading a file

*/

public class Solution {
	public static void main(String[] args) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String fileName = reader.readLine();
		reader.close();

		File file = new File(fileName);
		FileInputStream fileInputStream = new FileInputStream(file);
		int content;
		while ((content = fileInputStream.read()) != -1) {
			System.out.print((char) content);
		}

		fileInputStream.close();
	}
}