package com.codegym.task.task18.task1821;

/* 
Symbol frequency

*/

import java.io.FileInputStream;
import java.io.IOException;

public class Solution {
	public static void main(String[] args) throws IOException {
        String fileName = args[0];
//		String fileName = "resources/lorem.txt";

		FileInputStream inputStream = new FileInputStream(fileName);

		short[] storage = new short[256];
		while (inputStream.available() > 0) {
			int readByte = inputStream.read();
			storage[readByte]++;
		}

		inputStream.close();

		for (int i = 0; i < storage.length; i++) {
			if (storage[i] > 0) {
				System.out.println(((char) i) + " " + storage[i]);
			}
		}
	}
}
