package com.codegym.task.task18.task1804;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

/* 
Rarest bytes

*/

public class Solution {
	public static void main(String[] args) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String fileName = reader.readLine();
		reader.close();

		FileInputStream inputStream = new FileInputStream(fileName);
		int[] counter = new int[256];
		while (inputStream.available() > 0) {
			int readByte = inputStream.read();
			counter[readByte]++;
		}
		inputStream.close();

		Set<Integer> rarestBytes = new HashSet<>();
		int min = 0;
		boolean isFirst = true;
		for (int i = 0; i < counter.length; i++) {
			if (counter[i] > 0) {
				if (isFirst) {
					min = counter[i];
					rarestBytes.add(i);
					isFirst = false;
				} else if (counter[i] < min) {
					min = counter[i];
					rarestBytes.clear();
					rarestBytes.add(i);
				} else if (counter[i] == min) {
					rarestBytes.add(i);
				}
			}
		}

		for (Integer element : rarestBytes) {
			System.out.print(element + " ");
		}
	}
}
