package com.codegym.task.task18.task1823;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* 
Threads and bytes

*/

public class Solution {
	public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			String fileName = reader.readLine();
			if (fileName.equals("exit")) {
				break;
			}

			Thread thread = new ReadThread(fileName);
			thread.start();
		}
		reader.close();
	}

	public static class ReadThread extends Thread {
		private String fileName;

		public ReadThread(String fileName) {
			this.fileName = fileName;
		}

		@Override
		public void run() {
			try (FileInputStream inputStream = new FileInputStream(fileName)) {
				short[] counterArray = new short[256];
				while (inputStream.available() > 0) {
					int index = inputStream.read();
					counterArray[index]++;
				}

				int max = 0;
				int asciiCharacter = 0;
				boolean isFirst = true;
				for (int i = 0; i < 256; i++) {
					if (isFirst) {
						asciiCharacter = i;
						max = counterArray[i];
						isFirst = false;
					} else if (counterArray[i] > max) {
						max = counterArray[i];
						asciiCharacter = i;
					}
				}

				resultMap.put(fileName, asciiCharacter);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
