package com.codegym.task.task18.task1808;

/* 
Splitting a file

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) {
		String sourceFileName = null;
		String destinationFileNameA = null;
		String destinationFileNameB = null;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
			sourceFileName = reader.readLine();
			destinationFileNameA = reader.readLine();
			destinationFileNameB = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (FileInputStream inputStream = new FileInputStream(sourceFileName)) {
			if (inputStream.available() >= 0) {
				byte[] buffer = new byte[inputStream.available()];
				int count = inputStream.read(buffer);

				int partA;
				int partB;
				partA = partB = count / 2;
				if (count % 2 != 0) {
					partA++;
				}

				FileOutputStream outputStreamA = new FileOutputStream(destinationFileNameA);
				outputStreamA.write(buffer, 0, partA);

				FileOutputStream outputStreamB = new FileOutputStream(destinationFileNameB);
				outputStreamB.write(buffer, partA, partB);

				outputStreamA.close();
				outputStreamB.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
