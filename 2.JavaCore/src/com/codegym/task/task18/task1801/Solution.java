package com.codegym.task.task18.task1801;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* 
Maximum byte

*/

public class Solution {
	public static void main(String[] args) throws Exception {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String fileName = bufferedReader.readLine();
		bufferedReader.close();

		FileInputStream fileInputStream = new FileInputStream(fileName);
		int max = 0;
		boolean isFirst = true;
		while (fileInputStream.available() > 0) {
			if (isFirst) {
				max = fileInputStream.read();
				isFirst = false;
			} else {
				int readed = fileInputStream.read();
				if (readed > max) {
					max = readed;
				}
			}
		}
		fileInputStream.close();

		System.out.println(max);
	}
}
