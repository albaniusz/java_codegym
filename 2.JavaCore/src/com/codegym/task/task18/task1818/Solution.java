package com.codegym.task.task18.task1818;

/* 
Two in one

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) throws IOException {
		String fileA, fileB, fileC = null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		fileA = reader.readLine();
		fileB = reader.readLine();
		fileC = reader.readLine();
		reader.close();

		FileInputStream inputStreamB = new FileInputStream(fileB);
		byte[] contentB = null;
		if (inputStreamB.available() >= 0) {
			contentB = new byte[inputStreamB.available()];
			inputStreamB.read(contentB);
		}

		FileInputStream inputStreamC = new FileInputStream(fileC);
		byte[] contentC = null;
		if (inputStreamC.available() >= 0) {
			contentC = new byte[inputStreamC.available()];
			inputStreamC.read(contentC);
		}

		FileOutputStream outputStreamA = new FileOutputStream(fileA);
		outputStreamA.write(contentB);
		outputStreamA.write(contentC);
		outputStreamA.close();

		inputStreamB.close();
		inputStreamC.close();
	}
}
