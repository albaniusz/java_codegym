package com.codegym.task.task18.task1807;

/* 
Counting commas

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) {
		String fileName = null;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
			fileName = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		int counter = 0;
		try (FileInputStream inputStream = new FileInputStream(fileName)) {
			while (inputStream.available() > 0) {
				int readByte = inputStream.read();
				if (readByte == 44) {
					counter++;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.print(counter);
	}
}
