package com.codegym.task.task18.task1812;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Extending AmigoOutputStream

*/

public class QuestionFileOutputStream implements AmigoOutputStream {
	private AmigoOutputStream amigoOutputStream;

	public QuestionFileOutputStream(AmigoOutputStream amigoOutputStream) {
		this.amigoOutputStream = amigoOutputStream;
	}

	@Override
	public void flush() throws IOException {
		amigoOutputStream.flush();
	}

	@Override
	public void write(int b) throws IOException {
		amigoOutputStream.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException {
		amigoOutputStream.write(b);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		amigoOutputStream.write(b, off, len);
	}

	@Override
	public void close() throws IOException {
		System.out.print("Do you really want to close the stream? Y/N");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String input = reader.readLine();
		reader.close();
		if (input.equals("Y")) {
			amigoOutputStream.close();
		}
	}
}

