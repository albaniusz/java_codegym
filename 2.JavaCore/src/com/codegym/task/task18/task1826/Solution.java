package com.codegym.task.task18.task1826;

/* 
Encryption

*/

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Solution {
	public static void main(String[] args) throws IOException {
		FileInputStream inputStream = new FileInputStream(args[1]);
		FileOutputStream outputStream = new FileOutputStream(args[2]);

		while (inputStream.available() > 0) {
			int readByte = inputStream.read();
			readByte = args[0].equals("-e") ? readByte + 250 : readByte - 250;
			outputStream.write(readByte);
		}

		outputStream.close();
		inputStream.close();
	}
}
