package com.codegym.task.task18.task1819;

/* 
Combining files

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) throws IOException {
		String fileNameA, fileNameB = null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		fileNameA = reader.readLine();
		fileNameB = reader.readLine();
		reader.close();

		FileInputStream inputStreamA = new FileInputStream(fileNameA);
		byte[] contentA = new byte[inputStreamA.available()];
		if (inputStreamA.available() > 0) {
			inputStreamA.read(contentA);
		}
		inputStreamA.close();

		FileInputStream inputStreamB = new FileInputStream(fileNameB);
		byte[] contentB = new byte[inputStreamB.available()];
		if (inputStreamB.available() >= 0) {
			inputStreamB.read(contentB);
		}
		inputStreamB.close();

		FileOutputStream outputStreamA = new FileOutputStream(fileNameA);
		outputStreamA.write(contentB);
		outputStreamA.write(contentA);
		outputStreamA.close();
	}
}
