package com.codegym.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.TreeSet;

/* 
Sorting bytes

*/

public class Solution {
	public static void main(String[] args) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
		reader.close();

		FileInputStream inputStream = new FileInputStream(fileName);
		Set<Integer> readBytes = new TreeSet<>();
		while (inputStream.available() > 0) {
			readBytes.add(inputStream.read());
		}
		inputStream.close();

		for (Integer element : readBytes) {
			System.out.print(element + " ");
		}
	}
}
