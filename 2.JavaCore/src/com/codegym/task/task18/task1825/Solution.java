package com.codegym.task.task18.task1825;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.TreeSet;

/* 
Building a file

*/

public class Solution {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		Set<String> fileNames = new TreeSet<>();
		String lastFileName = "";
//		while (true) {
//			String fileName = reader.readLine();
//			if (fileName.equals("end")) {
//				break;
//			}
//			lastFileName = fileName;
//		}
		reader.close();


		fileNames.add("resources/task1825.txt.part2");
		fileNames.add("resources/task1825.txt.part3");
		fileNames.add("resources/task1825.txt.part1");
		lastFileName = "resources/task1825.txt.part1";


		String[] splited = lastFileName.split(".part\\d$");
		FileOutputStream outputStream = new FileOutputStream(splited[0]);

		for (String fileName : fileNames) {
			FileInputStream inputStream = new FileInputStream(fileName);



//			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
//			String line;
//			while ((line = br.readLine()) != null) {
//				outputStream.write(line.getBytes());
//			}
//			br.close();
//			outputStream.flush();




//			byte[] buffer = new byte[2048];
//			while (inputStream.available() > 0) {
//				int bytes = inputStream.read(buffer);
//				outputStream.write(buffer, 0, bytes);
//			}
//			inputStream.close();
//			outputStream.flush();
		}

		outputStream.close();
	}
}
