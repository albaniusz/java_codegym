package com.codegym.task.task18.task1820;

/* 
Rounding numbers

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) throws IOException {
		String fileNameA, fileNameB = null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		fileNameA = reader.readLine();
		fileNameB = reader.readLine();
		reader.close();

//		fileNameA = "resources/task1820A.txt";
//		fileNameB = "resources/task1820B.txt";

		FileInputStream inputStream = new FileInputStream(fileNameA);
		StringBuilder contentBuilder = new StringBuilder();
		int i = 0;
		while ((i = inputStream.read()) != -1) {
			contentBuilder.append((char) i);
		}
		inputStream.close();

		String[] exploded = contentBuilder.toString().split(" ");

		StringBuilder contentToWrite = new StringBuilder();
		for (String element : exploded) {
			contentToWrite.append(Math.round(Float.parseFloat(element))).append(" ");
		}

		FileOutputStream outputStream = new FileOutputStream(fileNameB);
		outputStream.write(contentToWrite.toString().getBytes());
		outputStream.close();
	}
}
