package com.codegym.task.task18.task1809;

/* 
Reversing a file

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) {
		String fileNameA = null;
		String fileNameB = null;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
			fileNameA = reader.readLine();
			fileNameB = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

//		fileNameA = "lorem.txt";
//		fileNameB = "task1809.txt";

		try (FileInputStream inputStream = new FileInputStream(fileNameA)) {
			if (inputStream.available() >= 0) {
				byte[] buffer = new byte[inputStream.available()];
				int count = inputStream.read(buffer);

				FileOutputStream outputStream = new FileOutputStream(fileNameB);
				for (int i = buffer.length - 1; i >= 0; i--) {
					outputStream.write(buffer[i]);
				}
				outputStream.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
