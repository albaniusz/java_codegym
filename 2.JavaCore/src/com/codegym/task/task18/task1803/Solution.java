package com.codegym.task.task18.task1803;

/*
Most frequent bytes

*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Solution {
	public static void main(String[] args) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String fileName = reader.readLine();
		reader.close();

		FileInputStream inputStream = new FileInputStream(fileName);
		Map<Integer, Integer> counterMap = new HashMap<>();
		while (inputStream.available() > 0) {
			int readByte = inputStream.read();
			if (counterMap.containsKey(readByte)) {
				counterMap.replace(readByte, counterMap.get(readByte) + 1);
			} else {
				counterMap.put(readByte, 1);
			}
		}
		inputStream.close();

		int max = 0;
		boolean isFirst = true;
		Set<Integer> keys = new HashSet<>();
		for (Map.Entry<Integer, Integer> entry : counterMap.entrySet()) {
			if (isFirst) {
				keys.add(entry.getKey());
				max = entry.getValue();
				isFirst = false;
			} else if (entry.getValue() == max) {
				keys.add(entry.getKey());
			} else {
				if (entry.getValue() > max) {
					max = entry.getValue();
					keys.clear();
					keys.add(entry.getKey());
				}
			}
		}

		for (Integer element : keys) {
			System.out.print(element + " ");
		}
	}
}
