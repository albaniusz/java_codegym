package com.codegym.task.task18.task1802;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* 
Minimum byte

*/

public class Solution {
	public static void main(String[] args) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String fileName = reader.readLine();
		reader.close();

		FileInputStream stream = new FileInputStream(fileName);
		int min = 0;
		boolean isFirst = true;
		while (stream.available() > 0) {
			if (isFirst) {
				min = stream.read();
				isFirst = false;
			} else {
				int readed = stream.read();
				if (readed < min) {
					min = readed;
				}
			}
		}
		stream.close();

		System.out.println(min);
	}
}
