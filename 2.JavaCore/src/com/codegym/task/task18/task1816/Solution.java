package com.codegym.task.task18.task1816;

/* 
ABCs

*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Solution {
	public static void main(String[] args) {
		String fileName = args[0];
//		String fileName = "lorem.txt";
		int counter = 0;

		try (FileInputStream inputStream = new FileInputStream(fileName)) {
			while (inputStream.available() > 0) {
				int readByte = inputStream.read();
				if (readByte >= 65 && readByte <= 90 || readByte >= 97 && readByte <= 122) {
					counter++;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.print(counter);
	}
}
