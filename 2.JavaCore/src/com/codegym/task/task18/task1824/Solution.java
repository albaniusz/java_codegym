package com.codegym.task.task18.task1824;

/* 
Files and exceptions

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) {
		String fileName = null;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
			while (true) {
				fileName = reader.readLine();
				try (FileInputStream inputStream = new FileInputStream(fileName)) {

				} catch (FileNotFoundException e) {
					System.out.print(fileName);
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
