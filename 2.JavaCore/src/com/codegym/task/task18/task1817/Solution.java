package com.codegym.task.task18.task1817;

/* 
Spaces

*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Solution {
	public static void main(String[] args) {
		String fileName = args[0];
		int counterSpace = 0;
		int counterCharacters = 0;

		try (FileInputStream inputStream = new FileInputStream(fileName)) {
			while (inputStream.available() > 0) {
				int readByte = inputStream.read();
				if (readByte == 32) {
					counterSpace++;
				}
				counterCharacters++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		double ratio = (double) counterSpace / (double) counterCharacters * 100.0;
		System.out.print(Math.round(ratio * 100.0) / 100.0);
	}
}
