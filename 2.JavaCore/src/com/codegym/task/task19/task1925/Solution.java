package com.codegym.task.task19.task1925;

/* 
Long words

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) throws IOException {
		String fileNameA = args[0];
		String fileNameB = args[1];

		BufferedReader bufferedReader = new BufferedReader(new FileReader(fileNameA));
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileNameB));

		boolean isFirst = true;
		StringBuilder resultsStringBuilder = new StringBuilder();
		while (bufferedReader.ready()) {
			String line = bufferedReader.readLine();
			String[] words = line.split(" ");
			for (String word : words) {
				if (word.length() > 6) {
					if (isFirst) {
						isFirst = false;
					} else {
						resultsStringBuilder.append(",");
					}
					resultsStringBuilder.append(word);
				}
			}

		}

        bufferedWriter.write(resultsStringBuilder.toString());
		bufferedWriter.close();
		bufferedReader.close();
	}
}
