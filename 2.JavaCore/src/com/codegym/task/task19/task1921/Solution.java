package com.codegym.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* 
John Johnson

*/

public class Solution {
	public static final List<Person> PEOPLE = new ArrayList<>();

	public static void main(String[] args) throws IOException, ParseException {
        String fileName = args[0];

		BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
		SimpleDateFormat formatter = new SimpleDateFormat("MM dd yyyy");
		while (bufferedReader.ready()) {
			String line = bufferedReader.readLine();
			String[] exploded = line.split(" ");
			StringBuilder stringBuilderName = new StringBuilder();
			StringBuilder stringBuilderDate = new StringBuilder();
			for (int i = 0; i < exploded.length; i++) {
				if (i >= exploded.length - 3) {
					stringBuilderDate.append(" ");
					stringBuilderDate.append(exploded[i]);
				} else {
					stringBuilderName.append(" ");
					stringBuilderName.append(exploded[i]);
				}
			}

			Date birthDate = formatter.parse(stringBuilderDate.toString().trim());
			Person person = new Person(stringBuilderName.toString().trim(), birthDate);
            PEOPLE.add(person);
		}
		bufferedReader.close();
	}
}
