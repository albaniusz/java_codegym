package com.codegym.task.task19.task1920;

/* 
The richest

*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Solution {
	public static void main(String[] args) throws IOException {
		String fileName = args[0];

		Map<String, Double> personsWithSalary = new TreeMap<>();
		BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
		while (bufferedReader.ready()) {
			String line = bufferedReader.readLine();
			String[] exploded = line.split(" ");

			if (personsWithSalary.containsKey(exploded[0])) {
				Double value = personsWithSalary.get(exploded[0]);
				personsWithSalary.put(exploded[0], value + Double.parseDouble(exploded[1]));
			} else {
				personsWithSalary.put(exploded[0], Double.parseDouble(exploded[1]));
			}
		}
		bufferedReader.close();

		Set<String> results = new TreeSet<>();
		double max = 0.0;
		for (Map.Entry<String, Double> entry : personsWithSalary.entrySet()) {
			if (entry.getValue() > max) {
				max = entry.getValue();
				results.clear();
				results.add(entry.getKey());
			} else if (entry.getValue() == max) {
				results.add(entry.getKey());
			}
		}

		for (String result : results) {
			System.out.println(result);
		}
	}
}
