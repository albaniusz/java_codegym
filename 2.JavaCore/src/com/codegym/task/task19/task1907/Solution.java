package com.codegym.task.task19.task1907;

/* 
Counting words

*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Solution {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String fileName = reader.readLine();
		reader.close();

//		String fileName = "resources/task1907.txt";

		String needle = "world";
		Scanner scanner = new Scanner(new FileReader(fileName));
		scanner.useDelimiter("\\W+");
		short counter = 0;
		while (scanner.hasNext()) {
			String read = scanner.next();
			if (read.equals(needle)) {
				counter++;
			}
		}
		scanner.close();

		System.out.print(counter);
	}
}
