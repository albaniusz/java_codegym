package com.codegym.task.task19.task1909;

/* 
Changing punctuation marks

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String fileNameA = reader.readLine();
		String fileNameB = reader.readLine();
		reader.close();

		BufferedReader fileReader = new BufferedReader(new FileReader(fileNameA));
		BufferedWriter fileWriter = new BufferedWriter(new FileWriter(fileNameB));

		while (fileReader.ready()) {
			String line = fileReader.readLine();

			line = line.replace('.', '!');

			fileWriter.write(line);
			fileWriter.flush();
		}

		fileWriter.close();
		fileReader.close();
	}
}
