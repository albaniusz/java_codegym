package com.codegym.task.task19.task1919;

/* 
Calculating salaries

*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
	public static void main(String[] args) throws IOException {
		String fileName = args[0];

		Map<String, Double> results = new TreeMap<>();
		BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
		while (bufferedReader.ready()) {
			String line = bufferedReader.readLine();
			String[] exploded = line.split(" ");

			if (results.containsKey(exploded[0])) {
				Double value = results.get(exploded[0]);
				results.put(exploded[0], value + Double.parseDouble(exploded[1]));
			} else {
				results.put(exploded[0], Double.parseDouble(exploded[1]));
			}
		}
		bufferedReader.close();

		for (Map.Entry<String, Double> entry : results.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
	}
}
