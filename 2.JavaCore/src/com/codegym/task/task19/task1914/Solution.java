package com.codegym.task.task19.task1914;

/* 
Problem solving

*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
	public static TestString testString = new TestString();

	public static void main(String[] args) throws Exception {
		PrintStream consoleStream = System.out;

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		PrintStream stream = new PrintStream(outputStream);
		System.setOut(stream);

		testString.printSomething();

		String result = outputStream.toString();

		System.setOut(consoleStream);

		String[] exploded = result.split(" ");
		int numberA = Integer.parseInt(exploded[0]);
		int numberB = Integer.parseInt(exploded[2]);
		int mathResult = 0;
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(numberA);
		stringBuilder.append(" ");

		switch (exploded[1]) {
			case "+":
				stringBuilder.append("+");
				mathResult = numberA + numberB;
				break;
			case "-":
				stringBuilder.append("-");
				mathResult = numberA - numberB;
				break;
			case "*":
				stringBuilder.append("*");
				mathResult = numberA * numberB;
				break;
			default:
				throw new Exception("Wrong operator!");
		}

		stringBuilder.append(" ");
		stringBuilder.append(numberB);
		stringBuilder.append(" = ");
		stringBuilder.append(mathResult);

		System.out.println(stringBuilder.toString());
	}

	public static class TestString {
		public void printSomething() {
			System.out.println("3 + 6 = ");
		}
	}
}

