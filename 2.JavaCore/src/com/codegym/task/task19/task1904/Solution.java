package com.codegym.task.task19.task1904;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/* 
Yet another adapter

*/

public class Solution {

	public static void main(String[] args) throws IOException {
		PersonScannerAdapter personScannerAdapter = new PersonScannerAdapter(
				new Scanner(new File("resources/task1904.txt")));

		Person person = personScannerAdapter.read();
		System.out.print(person);
		personScannerAdapter.close();
	}

	public static class PersonScannerAdapter implements PersonScanner {
		private final Scanner fileScanner;

		public PersonScannerAdapter(Scanner scanner) {
			this.fileScanner = scanner;
		}

		@Override
		public Person read() throws IOException {
			if (!fileScanner.hasNext()) {
				return null;
			}

			String line = fileScanner.nextLine();
			String[] lineExploded = line.split(" ");
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date birthDate = null;
			try {
				birthDate = simpleDateFormat.parse(
						Integer.parseInt(lineExploded[4]) + "-" +
								Integer.parseInt(lineExploded[3]) + "-" +
								Integer.parseInt(lineExploded[5])
				);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			return new Person(lineExploded[2], lineExploded[0], lineExploded[1], birthDate);
		}

		@Override
		public void close() throws IOException {
			fileScanner.close();
		}
	}
}
