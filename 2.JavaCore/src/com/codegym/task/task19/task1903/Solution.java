package com.codegym.task.task19.task1903;

/* 
Adapting multiple interfaces

*/

import java.util.HashMap;
import java.util.Map;

public class Solution {
	public static Map<String, String> countries = new HashMap<>();

	static {
		countries.put("UA", "Ukraine");
		countries.put("US", "United States");
		countries.put("FR", "France");
	}

	public static void main(String[] args) {

	}

	public static class IncomeDataAdapter implements Customer, Contact {
		private IncomeData data;

		public IncomeDataAdapter(IncomeData data) {
			this.data = data;
		}

		@Override
		public String getCompanyName() {
			return data.getCompany();
		}

		@Override
		public String getCountryName() {
			return countries.get(data.getCountryCode());
		}

		@Override
		public String getName() {
			return data.getContactLastName() + ", " + data.getContactFirstName();
		}

		@Override
		public String getPhoneNumber() {
			StringBuilder builder = new StringBuilder();
			builder.append("+");
			builder.append(data.getCountryPhoneCode());

			StringBuilder partBuilder = new StringBuilder();
			int phoneNumber = data.getPhoneNumber();
			for (int i = 0; i < 10; i++) {
				partBuilder.append(phoneNumber % 10);
				phoneNumber /= 10;
				switch (i) {
					case 1:
						partBuilder.append("-");
						break;
					case 3:
						partBuilder.append("-");
						break;
					case 6:
						partBuilder.append(")");
						break;
					case 9:
						partBuilder.append("(");
						break;
				}
			}
			builder.append(partBuilder.reverse().toString());

			return builder.toString();
		}
	}


	public static interface IncomeData {
		String getCountryCode();        // For example: US

		String getCompany();            // For example: CodeGym Ltd.

		String getContactFirstName();   // For example: John

		String getContactLastName();    // For example: Smith

		int getCountryPhoneCode();      // For example: 1

		int getPhoneNumber();           // For example: 991234567
	}

	public static interface Customer {
		String getCompanyName();        // For example: CodeGym Ltd.

		String getCountryName();        // For example: United States
	}

	public static interface Contact {
		String getName();               // For example: Smith, John

		String getPhoneNumber();        // For example: +1(099)123-45-67
	}
}