package com.codegym.task.task19.task1906;

/* 
Even characters

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileNameA = reader.readLine();
        String fileNameB = reader.readLine();
		reader.close();

//		String fileNameA = "resources/task1906A.txt";
//		String fileNameB = "resources/task1906B.txt";

		FileReader fileReader = new FileReader(fileNameA);
		FileWriter fileWriter = new FileWriter(fileNameB);
		int counter = 1;
		while (fileReader.ready()) {
		    int readByte = fileReader.read();
			if (counter % 2 == 0) {
				fileWriter.write(readByte);
			}
			counter++;
		}
		fileWriter.close();
		fileReader.close();
	}
}
