package com.codegym.task.task19.task1910;

/* 
Punctuation

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String fileNameA = reader.readLine();
		String fileNameB = reader.readLine();
		reader.close();

		BufferedReader bufferedReader = new BufferedReader(new FileReader(fileNameA));
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileNameB));

		while (bufferedReader.ready()) {
			String line = bufferedReader.readLine();
			line = line.replaceAll("([\\p{Punct}])+", "");
			bufferedWriter.write(line);
			bufferedWriter.flush();
		}

		bufferedWriter.close();
		bufferedReader.close();
	}
}
