package com.codegym.task.task19.task1923;

/* 
Words with numbers

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) throws IOException {
		String fileNameA = args[0];
		String fileNameB = args[1];

		BufferedReader reader = new BufferedReader(new FileReader(fileNameA));
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileNameB));

		while (reader.ready()) {
			String line = reader.readLine();
			String[] words = line.split(" ");
			for (String word : words) {
				if (word.matches(".*\\d.*")) {
					writer.write(word + " ");
				}
			}
			writer.flush();
		}

		writer.close();
		reader.close();
	}
}
