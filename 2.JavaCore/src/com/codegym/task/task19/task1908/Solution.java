package com.codegym.task.task19.task1908;

/* 
Picking out numbers

*/

import java.io.*;

public class Solution {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String fileNameA = reader.readLine();
		String fileNameB = reader.readLine();
		reader.close();

		BufferedReader fileReader = new BufferedReader(new FileReader(fileNameA));
		BufferedWriter fileWriter = new BufferedWriter(new FileWriter(fileNameB));

		while (fileReader.ready()) {
			String line = fileReader.readLine();
			String[] lineExploded = line.split(" ");
			for (String element : lineExploded) {
				try {
					int parsedElement = Integer.parseInt(element);
					fileWriter.write("" + parsedElement + " ");
				} catch (NumberFormatException e) {
				}
			}
			fileWriter.flush();
		}

		fileWriter.close();
		fileReader.close();
	}
}
