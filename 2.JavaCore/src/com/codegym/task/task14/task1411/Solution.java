package com.codegym.task.task14.task1411;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
User, loser, coder and programmer

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Person person = null;
        String key = null;

        mainLoop:
        while (true) {
            key = reader.readLine();
            switch (key) {
                case Person.USER:
                    person = new Person.User();
                    break;
                case Person.LOSER:
                    person = new Person.Loser();
                    break;
                case Person.CODER:
                    person = new Person.Coder();
                    break;
                case Person.PROGRAMMER:
                    person = new Person.Programmer();
                    break;
                default:
                    break mainLoop;
            }

            doWork(person);
        }
    }

    public static void doWork(Person person) {
        if (person instanceof Person.User) {
            ((Person.User) person).live();
        } else if (person instanceof Person.Loser) {
            ((Person.Loser) person).doNothing();
        } else if (person instanceof Person.Coder) {
            ((Person.Coder) person).writeCode();
        } else if (person instanceof Person.Programmer) {
            ((Person.Programmer) person).enjoy();
        }
    }
}
