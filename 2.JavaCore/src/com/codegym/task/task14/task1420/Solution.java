package com.codegym.task.task14.task1420;

/* 
GCD

*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
	public static void main(String[] args) throws Exception {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		int a = Integer.parseInt(bufferedReader.readLine());
		int b = Integer.parseInt(bufferedReader.readLine());
		bufferedReader.close();

		if (a <= 0 || b <= 0) {
			throw new Exception("Must be greater than 0");
		}

		int gcd = 1;
		int minNumber = a < b ? a : b;

		for (int i = minNumber; i > 1; i--) {
			if (a % i == 0 && b % i == 0) {
				gcd = i;
				break;
			}
		}

		System.out.println(gcd);
	}
}
