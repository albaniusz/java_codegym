package com.codegym.task.task14.task1407;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Player and Dancer

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Person person = null;
        String key;
        while (!(key = reader.readLine()).equals("exit")) {
            if ("player".equals(key)) {
                person = new Player();
            } else if ("dancer".equals(key)) {
                person = new Dancer();
            }
            haveFun(person);
        }
    }

    public static void haveFun(Person person) {
        person.playYourRole();
    }

    interface Person {
        void playYourRole();
    }

    static class Player implements Person {
        void play() {
            System.out.println("playing");
        }

        @Override
        public void playYourRole() {
            play();
        }
    }

    static class Dancer implements Person {
        void dance() {
            System.out.println("dancing");
        }

        @Override
        public void playYourRole() {
            dance();
        }
    }
}
