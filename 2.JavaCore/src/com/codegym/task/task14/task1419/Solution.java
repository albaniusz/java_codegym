package com.codegym.task.task14.task1419;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/* 
Exception invasion

*/

public class Solution {
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args) {
        initExceptions();

        for (Exception exception : exceptions) {
            System.out.println(exception);
        }
    }

    private static void initExceptions() {   // The first exception
        try {
            float i = 1 / 0;

        } catch (Exception e) {
            exceptions.add(e);
        }

        try {
            double x = Double.parseDouble("xcv");
        } catch (Exception e) {
            exceptions.add(e);
        }

        try {
            int[] array = {1, 2, 3};
            int x = array[4];
        } catch (Exception e) {
            exceptions.add(e);
        }

        try {
            BufferedReader reader = new BufferedReader(new FileReader("none.txt"));
        } catch (Exception e) {
            exceptions.add(e);
        }

        try {
            String lorem = null;
            lorem.toLowerCase();
        } catch (Exception e) {
            exceptions.add(e);
        }

        try {
            Object x = new Integer(0);
            System.out.println((String) x);
        } catch (Exception e) {
            exceptions.add(e);
        }

        try {
            String name = "name";
            name.charAt(5);
        } catch (Exception e) {
            exceptions.add(e);
        }

        try {
            Object x[] = new String[3];
            x[0] = new Integer(0);
        } catch (Exception e) {
            exceptions.add(e);
        }

        try {
            throw new NoSuchFieldException();
        } catch (Exception e) {
            exceptions.add(e);
        }

        try {
            throw new NoSuchMethodException();
        } catch (Exception e) {
            exceptions.add(e);
        }
    }
}



