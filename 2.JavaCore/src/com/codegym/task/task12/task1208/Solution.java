package com.codegym.task.task12.task1208;

/* 
Freedom of the press

*/

import java.util.Date;

public class Solution {
    public static void main(String[] args) {

    }

    public static void print() {
    }

    public static void print(String name) {
    }

    public static void print(String name, Date date) {
    }

    public static void print(String name, Date date, int number) {
    }

    public static void print(int number) {
    }
}
