package com.codegym.task.task12.task1233;

/* 
The isomorphs are coming

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        int[] data = new int[]{1, 2, 3, 5, -2, -8, 0, 77, 5, 5};

        Pair<Integer, Integer> result = getMinimumAndIndex(data);

        System.out.println("The minimum is " + result.x);
        System.out.println("The index of the minimum element is " + result.y);
    }

    public static Pair<Integer, Integer> getMinimumAndIndex(int[] array) {
        if (array == null || array.length == 0) {
            return new Pair<Integer, Integer>(null, null);
        }

        Integer minimum = null;
        Integer minimumIndex = null;
        for (int x = 0; x < array.length; x++) {
            if (minimum == null) {
                minimum = array[x];
                minimumIndex = x;
            }
            if (array[x] < minimum) {
                minimum = array[x];
                minimumIndex = x;
            }
        }

        return new Pair<Integer, Integer>(minimum, minimumIndex);
    }


    public static class Pair<X, Y> {
        public X x;
        public Y y;

        public Pair(X x, Y y) {
            this.x = x;
            this.y = y;
        }
    }
}
