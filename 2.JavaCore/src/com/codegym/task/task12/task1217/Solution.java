package com.codegym.task.task12.task1217;

/* 
Fly, run and swim

*/

public class Solution {
    public static void main(String[] args) {

    }

    public interface CanFly {
        public int getSomeNumber();
    }

    public interface CanRun {
        public int getSomeNumber();
    }

    public interface CanSwim {
        public int getSomeNumber();
    }
}
