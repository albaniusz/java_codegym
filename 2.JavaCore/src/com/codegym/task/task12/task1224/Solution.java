package com.codegym.task.task12.task1224;

/* 
Unknown animal

*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(getObjectType(new Cat()));
        System.out.println(getObjectType(new Tiger()));
        System.out.println(getObjectType(new Lion()));
        System.out.println(getObjectType(new Bull()));
        System.out.println(getObjectType(new Pig()));
    }

    public static String getObjectType(Object o) {
        String result = "Animal";

        if (o instanceof Cat) {
            result = "Cat";
        } else if (o instanceof Tiger) {
            result = "Tiger";
        } else if (o instanceof Lion) {
            result = "Lion";
        } else if (o instanceof Bull) {
            result = "Bull";
        }

        return result;
    }

    public static class Cat {
    }

    public static class Tiger {
    }

    public static class Lion {
    }

    public static class Bull {
    }

    public static class Pig {
    }
}
