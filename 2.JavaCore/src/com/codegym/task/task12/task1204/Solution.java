package com.codegym.task.task12.task1204;

/* 
Whether it's a bird or a lamp

*/

public class Solution {
    public static void main(String[] args) {
        printObjectType(new Cat());
        printObjectType(new Bird());
        printObjectType(new Lamp());
        printObjectType(new Cat());
        printObjectType(new Dog());
    }

    public static void printObjectType(Object o) {
        String result = "";
        if (o instanceof Cat) {
            result = "Cat";
        } else if (o instanceof Dog) {
            result = "Dog";
        } else if (o instanceof Bird) {
            result = "Bird";
        } else if (o instanceof Lamp) {
            result = "Lamp";
        }

        System.out.println(result);
    }

    public static class Cat {
    }

    public static class Dog {
    }

    public static class Bird {
    }

    public static class Lamp {
    }
}
