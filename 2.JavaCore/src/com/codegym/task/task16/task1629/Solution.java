package com.codegym.task.task16.task1629;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static volatile BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws InterruptedException {
        Read3Strings t1 = new Read3Strings();
        Read3Strings t2 = new Read3Strings();

        t1.start();
        t2.start();

        t1.join();
        t2.join();


        t1.printResult();
        t2.printResult();
    }

    public static class Read3Strings extends Thread {
        private List<String> list = new ArrayList<>(3);

        @Override
        public void run() {
            for (int x = 0; x < 3; x++) {
                try {
                    list.add(reader.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public void printResult() {
            for (String element : list) {
                System.out.print(element + " ");
            }
            System.out.print("\n");
        }
    }
}
