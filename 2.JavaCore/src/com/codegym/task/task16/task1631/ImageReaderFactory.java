package com.codegym.task.task16.task1631;

import com.codegym.task.task16.task1631.common.*;

public class ImageReaderFactory {
    public static ImageReader getImageReader(ImageTypes type) {
        if (type == null) {
            throw new IllegalArgumentException("Unknown image type");
        }

        ImageReader reader = null;
        switch (type) {
            case BMP:
                reader = new BmpReader();
                break;
            case JPG:
                reader = new JpgReader();
                break;
            case PNG:
                reader = new PngReader();
                break;
            default:
                throw new IllegalArgumentException("Unknown image type");
        }

        return reader;
    }
}
