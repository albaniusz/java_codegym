package com.codegym.task.task15.task1514;

import java.util.HashMap;
import java.util.Map;

/* 
Static modifiers: part 1

*/

public class Solution {
    public static Map<Double, String> labels = new HashMap<>();
    static {
        labels.put(1d, "A");
        labels.put(2d, "B");
        labels.put(3d, "C");
        labels.put(4d, "D");
        labels.put(5d, "E");
    }

    public static void main(String[] args) {
        System.out.println(labels);
    }
}
