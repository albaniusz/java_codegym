package com.codegym.task.task15.task1523;

/* 
Overloading constructors

*/

public class Solution {
    Solution() {
    }

    public Solution(int a) {
    }

    protected Solution(double a) {
    }

    private Solution(String a) {
    }

    public static void main(String[] args) {

    }
}

