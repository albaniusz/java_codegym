package com.codegym.task.task15.task1522;

/*
Reinforce the singleton pattern

*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) {

    }

    public static Planet thePlanet;

    static {
        readKeyFromConsoleAndInitPlanet();
    }

    public static void readKeyFromConsoleAndInitPlanet() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String line = "";
        try {
            line = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (Planet.EARTH.equals(line)) {
            thePlanet = Earth.getInstance();
        } else if (Planet.MOON.equals(line)) {
            thePlanet = Moon.getInstance();
        } else if (Planet.SUN.equals(line)) {
            thePlanet = Sun.getInstance();
        } else {
            thePlanet = null;
        }
    }
}
