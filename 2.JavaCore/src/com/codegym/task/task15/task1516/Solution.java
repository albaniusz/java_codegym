package com.codegym.task.task15.task1516;

/* 
Default values

*/

public class Solution {

     int intVar = 1;
     double doubleVar = 1d;
     Double DoubleVar = 1d;
     boolean booleanVar = false;
     Object ObjectVar = "lorem";
     Exception ExceptionVar = new Exception("lorem");
     String StringVar = "ipsum";

    public static void main(String[] args) {

        Solution dummy = new Solution();
        System.out.println(dummy.intVar);
        System.out.println(dummy.doubleVar);
        System.out.println(dummy.DoubleVar);
        System.out.println(dummy.booleanVar);
        System.out.println(dummy.ObjectVar);
        System.out.println(dummy.ExceptionVar);
        System.out.println(dummy.StringVar);

    }
}
