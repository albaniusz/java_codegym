package com.codegym.task.task15.task1519;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Different methods for different types

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String line;
        while (!(line = reader.readLine()).equals("exit")) {
            try {
                if (line.contains(".")) {
                    print(Double.parseDouble(line));
                } else {
                    int number = Integer.parseInt(line);
                    if (number > 0 && number < 128) {
                        print((short) number);
                    } else if (number <= 0 || number >= 128) {
                        print(number);
                    }
                }
            } catch (NumberFormatException e) {
                print("" + line);
            }
        }
    }

    public static void print(Double value) {
        System.out.println("This is a Double. Value: " + value);
    }

    public static void print(String value) {
        System.out.println("This is a String. Value: " + value);
    }

    public static void print(short value) {
        System.out.println("This is a short. Value: " + value);
    }

    public static void print(Integer value) {
        System.out.println("This is an Integer. Value: " + value);
    }
}
