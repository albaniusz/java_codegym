package com.codegym.task.task15.task1527;

/*
Request parser

*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
	public static void main(String[] args) {
		String url = null;
		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
			url = bufferedReader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (url == null) {
			return;
		}

		String[] parameters = url.split("\\?");
		String[] pairs = parameters[parameters.length - 1].split("\\&");

		StringBuilder stringBuilder = new StringBuilder();
		String objValue = null;

		for (int i = 0; i < pairs.length; i++) {
			String[] keyValue = pairs[i].split("=");
			stringBuilder.append(keyValue[0]);
			stringBuilder.append(" ");

			if (keyValue[0].equals("obj") && keyValue.length > 1) {
				objValue = keyValue[1];
			}
		}
		System.out.println(stringBuilder.toString());

		if (objValue != null && !objValue.isEmpty()) {
			try {
				alert(Double.parseDouble(objValue));
			} catch (Exception e) {
				alert(objValue);
			}
		}
	}

	public static void alert(double value) {
		System.out.println("double: " + value);
	}

	public static void alert(String value) {
		System.out.println("String: " + value);
	}
}
