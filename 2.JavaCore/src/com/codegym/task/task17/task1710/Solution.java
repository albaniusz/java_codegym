package com.codegym.task.task17.task1710;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD

*/

public class Solution {
	public static List<Person> allPeople = new ArrayList<>();

	static {
		allPeople.add(Person.createMale("Donald Chump", new Date()));  // id=0
		allPeople.add(Person.createMale("Larry Gates", new Date()));  // id=1
	}

	public static void main(String[] args) throws ParseException {
		switch (args[0]) {
			case "-c":
				create(args);
				break;
			case "-u":
				update(args);
				break;
			case "-d":
				delete(args);
				break;
			case "-i":
				read(args);
				break;
		}

		System.out.println("Done!");
	}

	public static void create(String[] args) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH);

		for (int i = 1; i < args.length; i += 3) {
			String name = args[i];
			Date birthDate = simpleDateFormat.parse(args[i + 2]);

			Person person;
			if (args[i + 1].equals("m")) {
				person = Person.createMale(name, birthDate);
			} else {
				person = Person.createFemale(name, birthDate);
			}

			allPeople.add(person);
			System.out.print(allPeople.lastIndexOf(person));
		}
	}

	public static void read(String[] ids) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd yyyy", Locale.ENGLISH);

		for (String id : ids) {
			if (id.equals("-i")) {
				continue;
			}
			Person person = allPeople.get(Integer.parseInt(id));

			StringBuilder stringBuilder = new StringBuilder();

			String[] splitedName = person.getName().split(" ");
			stringBuilder.append(splitedName[splitedName.length - 1]);
			stringBuilder.append(" ");

			stringBuilder.append(person.getSex() == Sex.MALE ? "m" : "f");
			stringBuilder.append(" ");

			stringBuilder.append(simpleDateFormat.format(person.getBirthDate()));

			System.out.println(stringBuilder.toString());
		}
	}

	public static void update(String[] args) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH);

		for (int i = 1; i < args.length; i += 5) {
			Person person = allPeople.get(Integer.parseInt(args[i]));

			String name = args[i + 1];
			Sex sex = args[i + 2].equals("m") ? Sex.MALE : Sex.FEMALE;

			Date birthDate = simpleDateFormat.parse(args[i + 3]);

			person.setName(name);
			person.setSex(sex);
			person.setBirthDate(birthDate);
		}
	}

	public static void delete(String[] ids) {
		for (String id : ids) {
			if (id.equals("-d")) {
				continue;
			}
			Person person = allPeople.get(Integer.parseInt(id));
			person.setName(null);
			person.setSex(null);
			person.setBirthDate(null);
		}
	}
}
