package com.codegym.task.task17.task1721;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/* 
Transactionality

*/

public class Solution {
	public static List<String> allLines = new ArrayList<>();
	public static List<String> linesForRemoval = new ArrayList<>();

	public static void main(String[] args) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String allLinesFileName = bufferedReader.readLine();
		String linesForRemovalFileName = bufferedReader.readLine();
		bufferedReader.close();

		try (Stream<String> stream = Files.lines(Paths.get(allLinesFileName))) {
			stream.forEach(allLines::add);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (Stream<String> stream = Files.lines(Paths.get(linesForRemovalFileName))) {
			stream.forEach(linesForRemoval::add);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			Solution solution = new Solution();
			solution.joinData();
		} catch (CorruptedDataException e) {
			System.out.println(e.getMessage());
		}
	}

	public void joinData() throws CorruptedDataException {
		boolean isContainsAllElements = true;

		for (String element : Solution.linesForRemoval) {
			if (!Solution.allLines.contains(element)) {
				isContainsAllElements = false;
				break;
			}
		}

		if (isContainsAllElements) {
			Solution.allLines.removeAll(Solution.linesForRemoval);
		} else {
			Solution.allLines.clear();
			throw new CorruptedDataException();
		}
	}
}
