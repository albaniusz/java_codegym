package com.codegym.task.task17.task1714;

/* 
Comparable

*/

public class Beach implements Comparable<Beach> {
	private String name;
	private float distance;
	private int quality;

	public Beach(String name, float distance, int quality) {
		this.name = name;
		this.distance = distance;
		this.quality = quality;
	}

	public synchronized String getName() {
		return name;
	}

	public synchronized void setName(String name) {
		this.name = name;
	}

	public synchronized float getDistance() {
		return distance;
	}

	public synchronized void setDistance(float distance) {
		this.distance = distance;
	}

	public synchronized int getQuality() {
		return quality;
	}

	public synchronized void setQuality(int quality) {
		this.quality = quality;
	}

	public static void main(String[] args) {
		Beach beach1 = new Beach("B1", 1.5F, 2);
		Beach beach2 = new Beach("B2", 1.75F, 3);

		System.out.println(beach1.compareTo(beach1));
		System.out.println(beach1.compareTo(beach2));
		System.out.println(beach2.compareTo(beach1));


	}

	@Override
	public synchronized int compareTo(Beach o) {
		if (this == o
				|| this.equals(o)
				|| (int) getDistance() == (int) o.getDistance() && getQuality() == o.getQuality()) {
			return 0;
		}

		int result = (int) (getDistance() + (float) getQuality() - o.getDistance() + (float) o.getQuality());
		if (getDistance() + (float) getQuality() < o.getDistance() + (float) o.getQuality()) {
			result = -result;
		}

		return result;
	}
}
