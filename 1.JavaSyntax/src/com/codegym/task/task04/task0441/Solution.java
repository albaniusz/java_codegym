package com.codegym.task.task04.task0441;


/* 
Somehow average

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());

        int middle = 0;

        if (a == b || b == c || a == c) {
            if (a == b || a == c) {
                middle = a;
            } else {
                middle = b;
            }
        } else if ((a > b && a < c) || (a < b && a > c)) {
            middle = a;
        } else if ((b > a && b < c) || (b < a && b > c)) {
            middle = b;
        } else if ((c > a && c < b) || (c < a && c > b)) {
            middle = c;
        } else {
            middle = a;
        }
        System.out.println(middle);
    }
}
