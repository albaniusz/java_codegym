package com.codegym.task.task04.task0414;

/* 
Number of days in the year

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        int result = 0;
        int aLeapYear = 366;
        int anOrdinaryYear = 365;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int year = Integer.parseInt(reader.readLine());

        if (year % 400 == 0) {
            result = aLeapYear;
        } else if (year % 100 == 0) {
            result = anOrdinaryYear;
        } else if (year % 4 == 0) {
            result = aLeapYear;
        } else {
            result = anOrdinaryYear;
        }

        System.out.println("Number of days in the year: " + result);
    }
}
