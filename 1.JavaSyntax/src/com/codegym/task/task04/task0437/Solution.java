package com.codegym.task.task04.task0437;


/* 
Triangle of eights

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        for (int x = 1; x <= 10; x++) {
            for (int y = 0; y < x; y++) {
                System.out.print("8");
            }
            System.out.println();
        }
    }
}
