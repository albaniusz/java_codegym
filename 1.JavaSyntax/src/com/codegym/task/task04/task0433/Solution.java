package com.codegym.task.task04.task0433;


/* 
Seeing dollars in your future

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        int x = 0;
        while (x < 10) {
            int y = 0;
            while (y < 10) {
                System.out.print("$");
                y++;
            }
            x++;
            System.out.println();
        }

    }
}
