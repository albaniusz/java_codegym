package com.codegym.task.task04.task0416;

/* 
Crossing the road blindly

*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        double t = Double.parseDouble(reader.readLine());

        t = t % 10.0;
        if (t >= 5.0) {
            t -= 5.0;
        }

        String trafficLightColor = "red";

        if (t < 3d) {
            trafficLightColor = "green";
        } else if (t < 4d) {
            trafficLightColor = "yellow";
        }

        System.out.println(trafficLightColor);
    }
}