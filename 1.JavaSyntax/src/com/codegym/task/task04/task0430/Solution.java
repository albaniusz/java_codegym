package com.codegym.task.task04.task0430;

/* 
1 to 10

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        int x = 0;
        while (x < 10) {
            System.out.println(x + 1);
            x++;
        }
    }
}