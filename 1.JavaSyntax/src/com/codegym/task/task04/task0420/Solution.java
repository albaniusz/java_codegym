package com.codegym.task.task04.task0420;

/* 
Sorting three numbers

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int buffer = 0;

        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());


        if (b > a) {
            buffer = a;
            a = b;
            b = buffer;
        }

        if (c > b) {
            buffer = b;
            b = c;
            c = buffer;
            if (b > a) {
                buffer = a;
                a = b;
                b = buffer;
            }
        }

        System.out.println(a + " " + b + " " + c);
    }
}
