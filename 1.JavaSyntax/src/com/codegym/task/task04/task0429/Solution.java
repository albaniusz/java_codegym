package com.codegym.task.task04.task0429;

/* 
Positive and negative numbers

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int positiveCounter = 0;
        int negativeCounter = 0;

        for (int x = 0; x < 3; x++) {
            int number = Integer.parseInt(reader.readLine());
            if (number < 0) {
                negativeCounter++;
            } else if (number > 0) {
                positiveCounter++;
            }
        }

        System.out.println("Number of negative numbers: " + negativeCounter);
        System.out.println("Number of positive numbers: " + positiveCounter);
    }
}
