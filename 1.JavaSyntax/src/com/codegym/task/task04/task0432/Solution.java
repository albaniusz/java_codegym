package com.codegym.task.task04.task0432;



/* 
You can't have too much of a good thing

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String phrase = reader.readLine();
        int number = Integer.parseInt(reader.readLine());

        int x = 0;
        while (x < number) {
            System.out.println(phrase);
            x++;
        }
    }
}
