package com.codegym.task.task04.task0409;

/* 
Closest to 10

*/

public class Solution {
    public static void main(String[] args) {
        displayClosestToTen(8, 11);
        displayClosestToTen(7, 14);
    }

    public static void displayClosestToTen(int a, int b) {
        int aToTen = abs(10 - a);
        int bToTen = abs(10 - b);
        int result = 0;

        if (aToTen < bToTen) {
            result = a;
        } else {
            result = b;
        }

//        System.out.println(a + " and " + b + ", " + result + " is closest to ten");
        System.out.println(result);
    }

    public static int abs(int a) {
        if (a < 0) {
            return -a;
        } else {
            return a;
        }
    }
}