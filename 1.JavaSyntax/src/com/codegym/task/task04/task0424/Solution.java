package com.codegym.task.task04.task0424;

/* 
Three numbers

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int index = 0;

        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());

        if (a == b) {
            index = 3;
        }
        if (a == c) {
            index = 2;
        }
        if (b == c) {
            index = 1;
        }

        if (index != 0) {
            System.out.println(index);
        }
    }
}
