package com.codegym.task.task04.task0428;

/* 
Positive number

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int counter = 0;

        int a = Integer.parseInt(reader.readLine());
        if (a > 0) {
            counter++;
        }

        int b = Integer.parseInt(reader.readLine());
        if (b > 0) {
            counter++;
        }

        int c = Integer.parseInt(reader.readLine());
        if (c > 0) {
            counter++;
        }

        System.out.println(counter);
    }
}
