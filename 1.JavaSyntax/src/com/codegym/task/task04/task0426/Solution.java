package com.codegym.task.task04.task0426;

/* 
Labels and numbers

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int number = Integer.parseInt(reader.readLine());

        String result = "";

        if (number > 0) {
            if (number % 2 == 0) {
                result = "Positive even number";
            } else {
                result = "Positive odd number";
            }
        } else if (number < 0) {
            if (number % 2 == 0) {
                result = "Negative even number";
            } else {
                result = "Negative odd number";
            }
        } else {
            result = "Zero";
        }

        System.out.println(result);
    }
}
