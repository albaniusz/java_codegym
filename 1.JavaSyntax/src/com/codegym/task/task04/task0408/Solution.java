package com.codegym.task.task04.task0408;

/* 
Good or bad?

*/

public class Solution {
    public static void main(String[] args) {
        compare(3);
        compare(6);
        compare(5);
    }

    public static void compare(int a) {
        int compareTo = 5;
        String resultMessage = "The number is equal to " + compareTo;
        if (a > compareTo) {
            resultMessage = "The number is greater than " + compareTo;
        } else if (a < compareTo) {
            resultMessage = "The number is less than " + compareTo;
        }

        System.out.println(resultMessage);
    }
}