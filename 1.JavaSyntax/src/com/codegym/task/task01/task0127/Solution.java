package com.codegym.task.task01.task0127;

/* 
Square of a number

*/

public class Solution {
    public static void main(String[] args) {
        int number = 5;
        System.out.println(sqr(number));
    }

    public static int sqr(int a) {
        return a * a;
    }
}
