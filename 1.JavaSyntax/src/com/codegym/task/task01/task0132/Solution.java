package com.codegym.task.task01.task0132;

/* 
Sum of the digits of a three-digit number

*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) {
        System.out.println(sumDigitsInNumber(546));
    }

    public static int sumDigitsInNumber(int number) {
        int result = 0;
        for (int x = 0; x < 3; x++) {
            result += number % 10;
            number = number / 10;
        }
        return result;
    }
}