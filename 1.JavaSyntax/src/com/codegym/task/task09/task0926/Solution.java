package com.codegym.task.task09.task0926;

import java.util.ArrayList;

/* 
List of number arrays

*/

public class Solution {
    public static void main(String[] args) {
        ArrayList<int[]> list = createList();
        printList(list);
    }

    public static ArrayList<int[]> createList() {
        ArrayList<int[]> list = new ArrayList<>();

        int[] array1 = {1, 2, 3, 4, 5};
        list.add(array1);

        int[] array2 = {1, 2};
        list.add(array2);

        int[] array3 = {1, 2, 3, 4};
        list.add(array3);

        int[] array4 = {1, 2, 3, 4, 5, 6, 7};
        list.add(array4);

        int[] array5 = {};
        list.add(array5);

        return list;
    }

    public static void printList(ArrayList<int[]> list) {
        for (int[] array : list) {
            for (int x : array) {
                System.out.println(x);
            }
        }
    }
}
