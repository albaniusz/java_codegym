package com.codegym.task.task09.task0923;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Vowels and consonants

*/

public class Solution {
    public static char[] vowels = new char[]{'a', 'e', 'i', 'o', 'u'};

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String line = reader.readLine();
        char[] characters = line.toCharArray();

        String vowels = "";
        String notVowels = "";

        for (char element : characters) {
            if (element == ' ') {
            } else if (isVowel(element)) {
                vowels += element + " ";
            } else {
                notVowels += element + " ";
            }
        }

        System.out.println(vowels + "\n" + notVowels);
    }

    // The method checks whether a letter is a vowel
    public static boolean isVowel(char c) {
        c = Character.toLowerCase(c);  // Convert to lowercase

        for (char d : vowels)   // Look for vowels in the array
        {
            if (c == d)
                return true;
        }
        return false;
    }
}