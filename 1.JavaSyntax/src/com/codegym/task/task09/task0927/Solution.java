package com.codegym.task.task09.task0927;

import java.util.*;

/* 
Ten cats

*/

public class Solution {
    public static void main(String[] args) {
        Map<String, Cat> map = createMap();
        Set<Cat> set = convertMapToSet(map);
        printCatSet(set);
    }

    public static Map<String, Cat> createMap() {
        Map<String, Cat> map = new HashMap<>();

        map.put("AAA", new Cat("AAA"));
        map.put("BBB", new Cat("BBB"));
        map.put("CCC", new Cat("CCC"));
        map.put("DDD", new Cat("DDD"));
        map.put("EEE", new Cat("EEE"));
        map.put("FFF", new Cat("FFF"));
        map.put("GGG", new Cat("GGG"));
        map.put("HHH", new Cat("HHH"));
        map.put("III", new Cat("III"));
        map.put("JJJ", new Cat("JJJ"));

        return map;
    }

    public static Set<Cat> convertMapToSet(Map<String, Cat> map) {
        Set<Cat> cats = new HashSet<>();

        Iterator<Map.Entry<String, Cat>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Cat> entry = iterator.next();
            cats.add(entry.getValue());
        }

        return cats;
    }

    public static void printCatSet(Set<Cat> set) {
        for (Cat cat : set) {
            System.out.println(cat);
        }
    }

    public static class Cat {
        private String name;

        public Cat(String name) {
            this.name = name;
        }

        public String toString() {
            return "Cat " + this.name;
        }
    }


}
