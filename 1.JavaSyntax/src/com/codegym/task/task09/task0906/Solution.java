package com.codegym.task.task09.task0906;

/* 
Logging stack traces

*/

public class Solution {
    public static void main(String[] args) {
        log("In main method");
    }

    public static void log(String s) {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        String logMessage = stackTraceElements[2].getClassName() + ": " + stackTraceElements[2].getMethodName() + ": " + s;
        System.out.println(logMessage);
    }
}
