package com.codegym.task.task09.task0922;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* 
What's today's date?

*/

public class Solution {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        SimpleDateFormat alpha = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat beta = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);

        Date dateAlpha = alpha.parse(reader.readLine());

        System.out.println(beta.format(dateAlpha).toUpperCase());
    }
}
