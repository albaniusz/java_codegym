package com.codegym.task.task09.task0901;

/* 
Returning a stack trace

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        method1();
    }

    public static StackTraceElement[] method1() {
        method2();

        Thread.currentThread().getStackTrace();
        return Thread.currentThread().getStackTrace();
    }

    public static StackTraceElement[] method2() {
        method3();

        Thread.currentThread().getStackTrace();
        return Thread.currentThread().getStackTrace();
    }

    public static StackTraceElement[] method3() {
        method4();

        Thread.currentThread().getStackTrace();
        return Thread.currentThread().getStackTrace();
    }

    public static StackTraceElement[] method4() {
        method5();

        Thread.currentThread().getStackTrace();
        return Thread.currentThread().getStackTrace();
    }

    public static StackTraceElement[] method5() {

        Thread.currentThread().getStackTrace();
        return Thread.currentThread().getStackTrace();
    }
}
