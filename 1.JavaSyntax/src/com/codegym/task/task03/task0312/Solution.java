package com.codegym.task.task03.task0312;

/* 
Time conversion

*/

public class Solution {
    public static int convertToSeconds(int hour) {
        return 60 * 60 * hour;
    }

    public static void main(String[] args) {
        System.out.println(convertToSeconds(10));
        System.out.println(convertToSeconds(12));
    }
}
