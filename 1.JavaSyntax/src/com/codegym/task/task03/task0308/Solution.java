package com.codegym.task.task03.task0308;

/* 
Product of 10 numbers

*/

public class Solution {
    public static void main(String[] args) {
        int result = 1;
        for (int x = 1; x <= 10; x++) {
            result *= x;
        }
        System.out.println(result);
    }
}
