package com.codegym.task.task03.task0303;

/* 
Currency exchange

*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(convertEurToUsd(125, 1.2));
        System.out.println(convertEurToUsd(50, 1.2));
    }

    public static double convertEurToUsd(int eur, double exchangeRate) {
        return (double) eur * exchangeRate;
    }
}
