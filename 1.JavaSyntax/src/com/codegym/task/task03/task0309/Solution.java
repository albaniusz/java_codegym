package com.codegym.task.task03.task0309;

/* 
Sum of 5 numbers

*/

public class Solution {
    public static void main(String[] args) {
        int result = 0;
        for (int x = 1; x <= 5; x++) {
            result += x;
            System.out.println(result);
        }
    }
}
