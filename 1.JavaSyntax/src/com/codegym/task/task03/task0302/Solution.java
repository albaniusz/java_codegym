package com.codegym.task.task03.task0302;

/* 
Display right away

*/
public class Solution {
    public static void printString(String a) {
        System.out.println(a);
    }

    public static void main(String[] args) {
        printString("Hello, Amigo!");
    }
}
