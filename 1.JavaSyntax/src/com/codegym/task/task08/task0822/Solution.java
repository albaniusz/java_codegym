package com.codegym.task.task08.task0822;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Minimum of N numbers

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        List<Integer> integerList = getIntegerList();
        System.out.println(getMinimum(integerList));
    }

    public static int getMinimum(List<Integer> array) {
        int minimum = 0;
        boolean first = true;

        for (Integer element : array) {
            if (first) {
                minimum = element;
                first = false;
            } else if (minimum > element) {
                minimum = element;
            }
        }

        return minimum;
    }

    public static List<Integer> getIntegerList() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());

        ArrayList<Integer> list = new ArrayList<>();
        for (int x = 0; x < n; x++) {
            list.add(Integer.parseInt(reader.readLine()));
        }

        return list;
    }
}
