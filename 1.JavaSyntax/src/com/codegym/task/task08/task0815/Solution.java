package com.codegym.task.task08.task0815;

import java.util.HashMap;

/* 
Census

*/

public class Solution {
    public static HashMap<String, String> createMap() {
        HashMap<String, String> map = new HashMap<>();

        map.put("AAA", "AAA");
        map.put("AA1", "BBB");
        map.put("BBB", "BBB");
        map.put("CCC", "CCC");
        map.put("DDD", "DDD");
        map.put("DD1", "EEE");
        map.put("FFF", "FFF");
        map.put("HHH", "GGG");
        map.put("HH1", "GGG");
        map.put("HH2", "GGG");

        return map;
    }

    public static int getSameFirstNameCount(HashMap<String, String> map, String name) {
        int counter = 0;

        for (HashMap.Entry<String, String> pair : map.entrySet()) {
            if (pair.getValue().equals(name)) {
                counter++;
            }
        }

        return counter;
    }

    public static int getSameLastNameCount(HashMap<String, String> map, String lastName) {
        int counter = 0;

        if (map.containsKey(lastName)) {
            counter++;
        }

        return counter;
    }

    public static void main(String[] args) {

    }
}
