package com.codegym.task.task08.task0819;

import java.util.HashSet;
import java.util.Set;

/* 
Set of cats

*/

public class Solution {
    public static void main(String[] args) {
        Set<Cat> cats = createCats();

        Cat cat = null;
        for (Cat element : cats) {
            cat = element;
            break;
        }
        cats.remove(cat);

        printCats(cats);
    }

    public static Set<Cat> createCats() {
        Set<Cat> set = new HashSet<>();
        set.add(new Cat());
        set.add(new Cat());
        set.add(new Cat());

        return set;
    }

    public static void printCats(Set<Cat> cats) {
        for (Cat element : cats) {
            System.out.println(element);
        }
    }

    public static class Cat {

    }
}
