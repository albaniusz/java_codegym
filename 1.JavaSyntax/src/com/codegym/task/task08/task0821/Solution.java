package com.codegym.task.task08.task0821;

import java.util.HashMap;
import java.util.Map;

/* 
Shared last names and first names

*/

public class Solution {
    public static void main(String[] args) {
        Map<String, String> map = createPeopleList();
        printPeopleList(map);
    }

    public static Map<String, String> createPeopleList() {
        Map<String, String> map = new HashMap<>();

        map.put("AAA", "AAA");
        map.put("AAA", "BBB");
        map.put("AAA", "CCC");
        map.put("BBB", "DDD");
        map.put("CCC", "DDD");
        map.put("DDD", "DDD");
        map.put("EEE", "EEE");
        map.put("FFF", "FFF");
        map.put("GGG", "GGG");
        map.put("HHH", "HHH");
//        map.put("III", "III");
//        map.put("JJJ", "JJJ");

        return map;
    }

    public static void printPeopleList(Map<String, String> map) {
        for (Map.Entry<String, String> s : map.entrySet()) {
            System.out.println(s.getKey() + " " + s.getValue());
        }
    }
}
