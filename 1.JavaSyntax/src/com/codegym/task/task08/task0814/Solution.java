package com.codegym.task.task08.task0814;

import java.util.HashSet;
import java.util.Iterator;

/* 
Greater than 10? You're not a good fit for us

*/

public class Solution {
    public static HashSet<Integer> createSet() {
        HashSet<Integer> set = new HashSet<>();

        for (int x = 1; x <= 20; x++) {
            set.add(x);
        }

        return set;
    }

    public static HashSet<Integer> removeAllNumbersGreaterThan10(HashSet<Integer> set) {

        Iterator<Integer> iterator = set.iterator();
        while (iterator.hasNext()) {
            int element = iterator.next();
            if (element > 10) {
                iterator.remove();
            }
        }

        return set;
    }

    public static void main(String[] args) {

    }
}
