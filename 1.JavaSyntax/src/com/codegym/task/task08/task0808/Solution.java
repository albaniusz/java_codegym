package com.codegym.task.task08.task0808;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* 
10 thousand deletions and insertions

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        // ArrayList
        ArrayList arrayList = new ArrayList();
        insert10000(arrayList);
        get10000(arrayList);
        set10000(arrayList);
        remove10000(arrayList);

        // LinkedList
        LinkedList linkedList = new LinkedList();
        insert10000(linkedList);
        get10000(linkedList);
        set10000(linkedList);
        remove10000(linkedList);
    }

    public static void insert10000(List list) {
        for (int x = 0; x < 10000; x++) {
            list.add("x");
        }
    }

    public static void get10000(List list) {
        for (int x = 0; x < 10000; x++) {
            list.get(x);
        }
    }

    public static void set10000(List list) {
        for (int x = 0; x < 10000; x++) {
            list.set(x, "y");
        }
    }

    public static void remove10000(List list) {
        for (int x = 0; x < 10000; x++) {
            list.remove(0);
        }
    }
}
