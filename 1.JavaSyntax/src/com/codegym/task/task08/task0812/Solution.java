package com.codegym.task.task08.task0812;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Longest sequence

*/
public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Integer> list = new ArrayList<>();
        int sequenceCounter = 1;
        int maxSequenceCounter = 1;
        int lastElement = 0;

        for (int x = 0; x < 10; x++) {
            int element = Integer.parseInt(reader.readLine());
            list.add(element);

            if (lastElement != element) {
                lastElement = element;
                sequenceCounter = 1;
            } else {
                sequenceCounter++;
                if (sequenceCounter > maxSequenceCounter) {
                    maxSequenceCounter = sequenceCounter;
                }
            }
        }

        System.out.println(maxSequenceCounter);
    }
}