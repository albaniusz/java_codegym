package com.codegym.task.task08.task0827;

import java.util.Calendar;
import java.util.Date;

/* 
Working with dates

*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(isDateOdd("MAY 1 2013"));
    }

    public static boolean isDateOdd(String dateAsString) {
        Date date = new Date(dateAsString);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        boolean result = false;
        int n = calendar.get(Calendar.DAY_OF_YEAR);
        if (n % 2 != 0) {
            result = true;
        }

        return result;
    }
}
