package com.codegym.task.task08.task0824;

/* 
Make a family

*/

import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {

        Human grandfather1 = new Human();
        Human grandmother1 = new Human();

        Human grandfather2 = new Human();
        Human grandmother2 = new Human();

        Human father = new Human();
        Human mother = new Human();

        Human children1 = new Human();
        Human children2 = new Human();
        Human children3 = new Human();

        ArrayList<Human> list = new ArrayList<>();
        list.add(children1);
        list.add(children2);
        list.add(children3);
        father.children = list;
        mother.children = list;

        ArrayList<Human> grand1 = new ArrayList<>();
        grand1.add(father);
        grandfather1.children = grand1;
        grandmother1.children = grand1;

        ArrayList<Human> grand2 = new ArrayList<>();
        grand2.add(father);
        grandfather2.children = grand2;
        grandmother2.children = grand2;

        System.out.println(grandfather1);
        System.out.println(grandmother1);

        System.out.println(grandfather2);
        System.out.println(grandmother2);

        System.out.println(father);
        System.out.println(mother);

        System.out.println(children1);
        System.out.println(children2);
        System.out.println(children3);
    }

    public static class Human {
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children = new ArrayList<>();

        public String toString() {
            String text = "";
            text += "Name: " + this.name;
            text += ", sex: " + (this.sex ? "male" : "female");
            text += ", age: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0) {
                text += ", children: " + this.children.get(0).name;

                for (int i = 1; i < childCount; i++) {
                    Human child = this.children.get(i);
                    text += ", " + child.name;
                }
            }
            return text;
        }
    }

}
