package com.codegym.task.task08.task0829;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* 
Software update

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<String, String> addresses = new HashMap<>();
        while (true) {
            String town = reader.readLine();
            if (town.isEmpty()) break;

            String family = reader.readLine();
            if (family.isEmpty()) break;


            addresses.put(town, family);
        }


        String townName = reader.readLine();

        if (!townName.isEmpty()) {
            String familyName = addresses.get(townName);
            System.out.println(familyName);
        }
    }
}
