package com.codegym.task.task08.task0818;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* 
Only for the rich

*/

public class Solution {
    public static HashMap<String, Integer> createMap() {
        HashMap<String, Integer> map = new HashMap<>();

        map.put("Aaa1", 500);
        map.put("Aaa2", 499);
        map.put("Aaa3", 235);
        map.put("Aaa4", 600);
        map.put("Aaa5", 600);
        map.put("Aaa6", 123);
        map.put("Aaa7", 123);
        map.put("Aaa8", 123);
        map.put("Aaa9", 123);
        map.put("Aaa10", 800);

        return map;
    }

    public static void removeItemFromMap(HashMap<String, Integer> map) {
        int limit = 500;
        Set<Map.Entry<String, Integer>> mapSet = map.entrySet();
        Iterator<Map.Entry<String, Integer>> mapIterator = mapSet.iterator();
        while (mapIterator.hasNext()) {
            Map.Entry<String, Integer> entry = mapIterator.next();
            if (entry.getValue() < limit) {
                mapIterator.remove();
            }
        }
    }

    public static void main(String[] args) {
    }
}