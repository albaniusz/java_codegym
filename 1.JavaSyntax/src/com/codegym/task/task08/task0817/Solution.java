package com.codegym.task.task08.task0817;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* 
We don't need repeats

*/

public class Solution {
    public static HashMap<String, String> createMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("AAA", "AAA");
        map.put("BBB", "AAA");
        map.put("CCC", "BBB");
        map.put("DDD", "CCC");
        map.put("EEE", "DDD");
        map.put("FFF", "DDD");
        map.put("GGG", "DDD");
        map.put("HHH", "FFF");
        map.put("III", "GGG");
        map.put("JJJ", "GGG");

        return map;
    }

    public static void removeFirstNameDuplicates(Map<String, String> map) {
        Set<String> usedNamesList = new HashSet<>();
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> entry : copy.entrySet()) {
            if (!usedNamesList.contains(entry.getValue())) {
                usedNamesList.add(entry.getValue());
            } else {
                removeItemFromMapByValue(map, entry.getValue());
            }
        }
    }

    public static void removeItemFromMapByValue(Map<String, String> map, String value) {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair : copy.entrySet()) {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }

    public static void main(String[] args) {

    }
}
