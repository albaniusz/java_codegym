package com.codegym.task.task08.task0813;

import java.util.HashSet;
import java.util.Set;

/* 
20 words that start with the letter "L"

*/

public class Solution {
    public static Set<String> createSet() {
        HashSet<String> set = new HashSet<>();

        set.add("La");
        set.add("Lb");
        set.add("Lc");
        set.add("Ld");
        set.add("Le");

        set.add("Lf");
        set.add("Lg");
        set.add("Lh");
        set.add("Li");
        set.add("Lj");

        set.add("Lk");
        set.add("Ll");
        set.add("Lm");
        set.add("Ln");
        set.add("Lo");

        set.add("Lp");
        set.add("Lr");
        set.add("Ls");
        set.add("Lt");
        set.add("Lq");

        return set;
    }

    public static void main(String[] args) {

    }
}
