package com.codegym.task.task08.task0823;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Going national

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();

        String output = "";

        char[] characters = s.toCharArray();

        char lastChar = ' ';
        for (char element : characters) {
            if (lastChar == ' ' && element != ' ') {
                String xxx = Character.toString(element);
                output += xxx.toUpperCase();
            } else {
                output += Character.toString(element);
            }
            lastChar = element;
        }

        System.out.println(output);
    }
}
