package com.codegym.task.task08.task0816;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
Kind Emma and the summer holidays

*/

public class Solution {
    public static HashMap<String, Date> createMap() throws ParseException {
        DateFormat df = new SimpleDateFormat("MMMMM d yyyy", Locale.ENGLISH);
        HashMap<String, Date> map = new HashMap<String, Date>();

        map.put("Stallone10", df.parse("JANUARY 1 1980"));//not removed
        map.put("Stallone6", df.parse("MAY 31 1980"));//not removed
        map.put("Stallone1", df.parse("JUNE 1 1980"));
        map.put("Stallone3", df.parse("JUNE 1 1980"));
        map.put("Stallone4", df.parse("JUNE 12 1980"));
        map.put("Stallone7", df.parse("JULY 1 1980"));
        map.put("Stallone8", df.parse("JUNE 1 1980"));
        map.put("Stallone2", df.parse("August 31 1980"));
        map.put("Stallone9", df.parse("OCTOBER 1 1980"));//not removed
        map.put("Stallone5", df.parse("December 1 1980"));//not removed

        return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map) {
        int summerFrom = 6 - 1;
        int summerTo = 8 - 1;

        Set<Map.Entry<String, Date>> mapSet = map.entrySet();
        Iterator<Map.Entry<String, Date>> mapIterator = mapSet.iterator();
        while (mapIterator.hasNext()) {
            Map.Entry<String, Date> entry = mapIterator.next();
            int monthNum = entry.getValue().getMonth();
            if (monthNum >= summerFrom && monthNum <= summerTo) {
                mapIterator.remove();
            }
        }
    }

    public static void main(String[] args) throws ParseException {
        removeAllSummerPeople(createMap());
    }
}
