package com.codegym.task.task08.task0826;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Five winners

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] array = new int[20];
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(reader.readLine());
        }

        sort(array);

        System.out.println(array[0]);
        System.out.println(array[1]);
        System.out.println(array[2]);
        System.out.println(array[3]);
        System.out.println(array[4]);
    }

    public static void sort(int[] array) {
        boolean hasChanged = true;
        while (hasChanged) {
            hasChanged = false;
            for (int x = 0; x < array.length - 1; x++) {
                if (array[x] < array[x + 1]) {
                    int buffer = array[x];
                    array[x] = array[x + 1];
                    array[x + 1] = buffer;
                    hasChanged = true;
                }
            }
        }
    }
}
