package com.codegym.task.task10.task1011;

/* 
Big salary

*/

public class Solution {
    public static void main(String[] args) {
        String s = "I do not want to learn Java. I want a big salary";

        char[] characters = s.toCharArray();

        for (int x = 0; x < 43; x++) {
            String output = "";
            for (int y = x; y < characters.length; y++) {
                output += characters[y];
            }
            System.out.println(output);
        }
    }
}
