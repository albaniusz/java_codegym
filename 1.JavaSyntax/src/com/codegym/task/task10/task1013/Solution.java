package com.codegym.task.task10.task1013;

/* 
Human class constructors

*/

import java.util.Date;

public class Solution {
    public static void main(String[] args) {
    }

    public static class Human {
        private String name;
        private String surname;
        private int age;
        private Date birthdate;
        private int height;
        private int weight;

        public Human() {

        }

        public Human(String name) {
            this(name, null);
        }

        public Human(String name, String surname) {
            this(name, surname, 0);
        }

        public Human(String name, String surname, int age) {
            this(name, surname, age, null);
        }

        public Human(String name, String surname, int age, Date birthdate) {
            this(name, surname, age, birthdate, 0);
        }

        public Human(String name, String surname, int age, Date birthdate, int height) {
            this(name, surname, age, birthdate, height, 0);
        }

        public Human(String name, String surname, int age, Date birthdate, int height, int weight) {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.birthdate = birthdate;
            this.height = height;
            this.weight = weight;
        }

        public Human(Date birthdate) {
            this.birthdate = birthdate;
        }

        public Human(Date birthdate, int height) {
            this.birthdate = birthdate;
            this.height = height;
        }

        public Human(int height) {
            this.height = height;
        }
    }
}
