package com.codegym.task.task10.task1015;

import java.lang.reflect.Array;
import java.util.ArrayList;

/* 
Array of string lists

*/

public class Solution {
    public static void main(String[] args) {
        ArrayList<String>[] arrayOfStringList = createList();
        printList(arrayOfStringList);
    }

    public static ArrayList<String>[] createList() {
        ArrayList<String>[] arrayLists = new ArrayList[10];

        for (int x=0;x<10;x++) {
            ArrayList<String> stringArrayList = new ArrayList<>();
            stringArrayList.add("AAA");
            stringArrayList.add("BBB");
            stringArrayList.add("CCC");
            stringArrayList.add("DDD");
            stringArrayList.add("EEE");

            arrayLists[x] = stringArrayList;
        }

        return arrayLists;
    }

    public static void printList(ArrayList<String>[] arrayOfStringList) {
        for (ArrayList<String> list : arrayOfStringList) {
            for (String s : list) {
                System.out.println(s);
            }
        }
    }
}