package com.codegym.task.task10.task1012;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* 
Number of letters

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        // Alphabet
        String abc = "abcdefghijklmnopqrstuvwxyz";
        char[] abcArray = abc.toCharArray();

        ArrayList<Character> alphabet = new ArrayList<>();
        for (char letter : abcArray) {
            alphabet.add(letter);
        }

        // Read in strings
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            String s = reader.readLine();
            list.add(s.toLowerCase());
        }

        Map<Character, Integer> counters = new HashMap<>();
        for (Character element : abcArray) {
            counters.put(element, 0);
        }

        for (String element : list) {
            char[] chars = element.toCharArray();
            for (char character : chars) {
                if (counters.containsKey(character)) {
                    counters.put(character, counters.get(character) + 1);
                }
            }
        }

        for (Map.Entry<Character, Integer> entry : counters.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
}
