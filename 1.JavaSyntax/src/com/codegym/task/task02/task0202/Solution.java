package com.codegym.task.task02.task0202;

/* 
Where does a Person come from?

*/
public class Solution {
    public static void main(String[] args) {
        Person person = new Person();
    }

    public static class Person {
        String name;
        int age;
        int weight;
        int money;
    }
}
