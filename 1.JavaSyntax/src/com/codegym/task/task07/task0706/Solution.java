package com.codegym.task.task07.task0706;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Streets and houses

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        int[] array = new int[15];
        int evenCounter = 0;
        int oddCounter = 0;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int x = 0; x < 15; x++) {
            array[x] = Integer.parseInt(reader.readLine());
            if (x % 2 == 0) {
                evenCounter += array[x];
            } else {
                oddCounter += array[x];
            }
        }

        String result;
        if (evenCounter > oddCounter) {
            result = "Even-numbered houses have more residents.";
        } else {
            result = "Odd-numbered houses have more residents.";
        }

        System.out.println(result);
    }
}
