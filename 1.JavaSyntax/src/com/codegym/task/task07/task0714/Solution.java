package com.codegym.task.task07.task0714;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Words in reverse

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int x = 0; x < 5; x++) {
            list.add(reader.readLine());
        }

        list.remove(2);

        for (int x = list.size(); x > 0; x--) {
            System.out.println(list.get(x - 1));
        }
    }
}
