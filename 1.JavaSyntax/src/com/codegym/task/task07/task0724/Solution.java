package com.codegym.task.task07.task0724;

/* 
Family census

*/

public class Solution {
    public static void main(String[] args) {
        Human grandfather1 = new Human("Grandfather1", true, 65);
        Human grandfather2 = new Human("Grandfather2", true, 68);
        Human grandmother1 = new Human("Grandmother1", false, 70);
        Human grandmother2 = new Human("Grandmother2", false, 71);
        Human father = new Human("Father", true, 40, grandfather1, grandmother1);
        Human mother = new Human("Mother", false, 38, grandfather2, grandmother2);
        Human child1 = new Human("Child1", true, 12, father, mother);
        Human child2 = new Human("Child2", false, 11, father, mother);
        Human child3 = new Human("Child3", true, 110, father, mother);

        System.out.println(grandfather1);
        System.out.println(grandfather2);
        System.out.println(grandmother1);
        System.out.println(grandmother2);
        System.out.println(father);
        System.out.println(mother);
        System.out.println(child1);
        System.out.println(child2);
        System.out.println(child3);
    }

    public static class Human {
        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public Human(String name, boolean sex, int age, Human father, Human mother) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        public String toString() {
            String text = "";
            text += "Name: " + this.name;
            text += ", sex: " + (this.sex ? "male" : "female");
            text += ", age: " + this.age;

            if (this.father != null)
                text += ", father: " + this.father.name;

            if (this.mother != null)
                text += ", mother: " + this.mother.name;

            return text;
        }
    }
}