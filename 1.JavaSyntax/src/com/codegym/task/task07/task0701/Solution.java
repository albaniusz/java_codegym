package com.codegym.task.task07.task0701;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Maximum in an array

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        int[] array = initializeArray();
        int max = max(array);
        System.out.println(max);
    }

    public static int[] initializeArray() throws IOException {
        int[] array = new int[20];
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (int x = 0; x < 20; x++) {
            array[x] = Integer.parseInt(reader.readLine());
        }

        return array;
    }

    public static int max(int[] array) {
        int max = 0;
        boolean firstTime = true;
        for (int x = 0; x < array.length; x++) {
            if (firstTime) {
                max = array[x];
                firstTime = false;
            } else {
                if (array[x] > max) {
                    max = array[x];
                }
            }
        }

        return max;
    }
}
