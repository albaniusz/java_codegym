package com.codegym.task.task07.task0728;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
In decreasing order

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] array = new int[20];
        for (int i = 0; i < 20; i++) {
            array[i] = Integer.parseInt(reader.readLine());
        }

        sort(array);

        for (int x : array) {
            System.out.println(x);
        }
    }

    public static void sort(int[] array) {
        boolean hasChanged = true;
        while (hasChanged) {
            hasChanged = false;
            for (int x = 0; x < array.length - 1; x++) {
                if (array[x] < array[x + 1]) {
                    int buffer = array[x];
                    array[x] = array[x + 1];
                    array[x + 1] = buffer;
                    hasChanged = true;
                }
            }
        }
    }
}
