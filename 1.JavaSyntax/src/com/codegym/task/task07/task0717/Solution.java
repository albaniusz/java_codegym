package com.codegym.task.task07.task0717;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Duplicating words

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int x = 0; x < 10; x++) {
            list.add(reader.readLine());
        }

        ArrayList<String> result = doubleValues(list);

        for (String element : result) {
            System.out.println(element);
        }
    }

    public static ArrayList<String> doubleValues(ArrayList<String> list) {
        ArrayList<String> output = new ArrayList<>();

        for (String element : list) {
            output.add(element);
            output.add(element);
        }

        return output;
    }
}
