package com.codegym.task.task07.task0721;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Min and max in arrays

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int maximum = 0;
        int minimum = 0;

        int[] list = new int[20];
        for (int x = 0; x < 20; x++) {
            int number = Integer.parseInt(reader.readLine());
            list[x] = number;
            if (x == 0) {
                maximum = number;
                minimum = number;
            }
            if (number > maximum) {
                maximum = number;
            }
            if (number < minimum) {
                minimum = number;
            }
        }

        System.out.print(maximum + " " + minimum);
    }
}
