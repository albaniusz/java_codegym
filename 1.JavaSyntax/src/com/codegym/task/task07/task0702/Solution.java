package com.codegym.task.task07.task0702;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
String array in reverse order

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        String[] array = new String[10];

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (int x = 0; x < 8; x++) {
            array[x] = reader.readLine();
        }

        for (int x = 9; x >= 0; x--) {
            System.out.println(array[x]);
        }
    }
}