package com.codegym.task.task07.task0711;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Remove and insert

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int x = 0; x < 5; x++) {
            list.add(reader.readLine());
        }

        for (int x = 0; x < 13; x++) {
//            for (int y = 0; y < 3; y++) {
                String buffer = list.get(list.size() - 1);
                list.remove(buffer);
                list.add(0, buffer);
//            }
        }

        for (String element : list) {
            System.out.println(element);
        }
    }
}
