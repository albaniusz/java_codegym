package com.codegym.task.task07.task0720;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Shuffled just in time

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        int m = Integer.parseInt(reader.readLine());

        ArrayList<String> list = new ArrayList<>();
        for (int x = 0; x < n; x++) {
            list.add(reader.readLine());
        }

        for (int x = 0; x < m; x++) {
            String buffer = list.get(0);
            list.remove(0);
            list.add(buffer);
        }

        for (String element : list) {
            System.out.println(element);
        }
    }
}
