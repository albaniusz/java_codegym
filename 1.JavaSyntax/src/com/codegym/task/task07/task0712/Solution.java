package com.codegym.task.task07.task0712;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Shortest or longest

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<>();

        String shortest = "";
        int shortestIndex = 0;
        String longest = "";
        int longestIndex = 0;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int x = 0; x < 10; x++) {
            String buffer = reader.readLine();
            list.add(buffer);

            if (x == 0) {
                shortest = buffer;
                shortestIndex = x;
                longest = buffer;
                longestIndex = x;
            } else {
                if (buffer.length() < shortest.length()) {
                    shortest = buffer;
                    shortestIndex = x;
                } else if (buffer.length() > longest.length()) {
                    longest = buffer;
                    longestIndex = x;
                }
            }
        }

        if (shortestIndex < longestIndex) {
            System.out.println(shortest);
        } else {
            System.out.println(longest);
        }
    }
}
