package com.codegym.task.task07.task0727;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Changing functionality

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        ArrayList<String> list = new ArrayList<String>();
        while (true) {
            String s = reader.readLine();
            if (s.isEmpty()) break;
            list.add(s);
        }

        ArrayList<String> result = new ArrayList<String>();
        for (String element : list) {
            if (element.length() % 2 == 0) {
                result.add(element + " " + element);
            } else {
                result.add(element + " " + element + " " + element);
            }
        }

        for (String element : result) {
            System.out.println(element);
        }
    }
}
