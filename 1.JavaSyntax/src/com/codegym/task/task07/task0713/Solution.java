package com.codegym.task.task07.task0713;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Playing Javarella

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<Integer> mainList = new ArrayList<>();
        ArrayList<Integer> listByThree = new ArrayList<>();
        ArrayList<Integer> listByTwo = new ArrayList<>();
        ArrayList<Integer> listOther = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int x = 0; x < 20; x++) {
            int number = Integer.parseInt(reader.readLine());
            mainList.add(number);
            if (number % 3 == 0) {
                listByThree.add(number);
            }
            if (number % 2 == 0) {
                listByTwo.add(number);
            }
            if (number % 2 != 0 && number % 3 != 0) {
                listOther.add(number);
            }
        }

        printList(mainList);
        printList(listByThree);
        printList(listByTwo);
        printList(listOther);
    }

    public static void printList(List<Integer> list) {
        for (Integer element : list) {
            System.out.println(element);
        }
    }
}
