package com.codegym.task.task07.task0708;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Longest string

*/

public class Solution {
    private static List<String> strings;

    public static void main(String[] args) throws Exception {
        strings = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int longestVal = 0;
        ArrayList<String> longest = new ArrayList<>();
        for (int x = 0; x < 5; x++) {
            String buffer = reader.readLine();

            if (longestVal < buffer.length()) {
                longestVal = buffer.length();
                longest.clear();
                longest.add(buffer);
            } else if (longestVal == buffer.length()) {
                longest.add(buffer);
            }

            strings.add(buffer);
        }

        for (String element : longest) {
            System.out.println(element);
        }
    }
}
