package com.codegym.task.task07.task0709;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Expressing ourselves more concisely

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> strings = new ArrayList<>();
        List<String> shortest = new ArrayList<>();
        int shortestLenght = 0;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int x = 0; x < 5; x++) {
            String buffer = reader.readLine();
            strings.add(buffer);

            if (shortestLenght == 0) {
                shortestLenght = buffer.length();
            }

            if (buffer.length() < shortestLenght) {
                shortestLenght = buffer.length();
                shortest.clear();
                shortest.add(buffer);
            } else if (buffer.length() == shortestLenght) {
                shortest.add(buffer);
            }
        }

        for (String element : shortest) {
            System.out.println(element);
        }
    }
}
