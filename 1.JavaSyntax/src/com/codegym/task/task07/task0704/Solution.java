package com.codegym.task.task07.task0704;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Flip the array

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] array = new int[10];

        for (int x = 0; x < 10; x++) {
            array[x] = Integer.parseInt(reader.readLine());
        }

        for (int x = 9; x >= 0; x--) {
            System.out.println(array[x]);
        }
    }
}
