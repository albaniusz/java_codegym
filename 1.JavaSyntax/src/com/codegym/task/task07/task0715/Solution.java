package com.codegym.task.task07.task0715;

import java.util.ArrayList;

/* 
More Sam-I-Am

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<>();

        list.add("Sam");
        list.add("I");
        list.add("Am");

        list.add(1, "Ham");
        list.add(3, "Ham");
        list.add("Ham");

        for (String element : list) {
            System.out.println(element);
        }
    }
}
