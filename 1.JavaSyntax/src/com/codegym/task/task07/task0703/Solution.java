package com.codegym.task.task07.task0703;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Lonely arrays interact

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        String[] arrayOfStrings = new String[10];
        int[] arrayOfNumbers = new int[10];

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int x = 0; x < 10; x++) {
            arrayOfStrings[x] = reader.readLine();
            arrayOfNumbers[x] = arrayOfStrings[x].length();
        }

        for (int x=0;x<10;x++) {
//            System.out.println(arrayOfStrings[x]);
            System.out.println(arrayOfNumbers[x]);
        }
    }
}
