package com.codegym.task.task07.task0705;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
One large array and two small ones

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        int[] bigArray = new int[20];
        int[] smallArrayA = new int[10];
        int[] smallArrayB = new int[10];

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int x = 0; x < 20; x++) {
            bigArray[x] = Integer.parseInt(reader.readLine());
        }

        for (int x = 0; x < 20; x++) {
            if (x < 10) {
                smallArrayA[x] = bigArray[x];
            } else {
                smallArrayB[x - 10] = bigArray[x];
            }
        }

        for (int element : smallArrayB) {
            System.out.println(element);
        }
    }
}
