package com.codegym.task.task05.task0520;

/* 
Create a Rectangle class

*/
public class Rectangle {
    int top;
    int left;
    int width;
    int height;

    public Rectangle(int width, int height) {
        this.top = 0;
        this.left = 0;
        this.width = width;
        this.height = height;
    }

    public Rectangle(int width) {
        this.top = 0;
        this.left = 0;
        this.width = width;
        this.height = width;
    }

    public Rectangle(int top, int left, int width) {
        this.top = top;
        this.left = left;
        this.width = width;
        this.height = width;
    }


    public Rectangle(int top, int left, int width, int height) {
        this.top = top;
        this.left = left;
        this.width = width;
        this.height = height;
    }

    public static void main(String[] args) {

    }
}
