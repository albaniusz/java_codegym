package com.codegym.task.task05.task0529;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Console-based piggy bank

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int amount = 0;

        while (true) {
            String text = reader.readLine();
            if (text.equals("sum") || text.equals("exit")) {
                break;
            }
            amount += Integer.parseInt(text);
        }

        System.out.println(amount);
    }
}
