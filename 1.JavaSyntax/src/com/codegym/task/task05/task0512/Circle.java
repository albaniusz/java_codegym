package com.codegym.task.task05.task0512;

/* 
Create a Circle class

*/

public class Circle {
    private int centerX;
    private int centerY;
    private int radius;
    private int width;
    private int color;


    public void initialize(int centerX, int centerY, int radius) {
        initialize(centerX, centerY, radius, 0);
    }

    public void initialize(int centerX, int centerY, int radius, int width) {
        initialize(centerX, centerY, radius, width, 0);
    }

    public void initialize(int centerX, int centerY, int radius, int width, int color) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.width = width;
        this.color = color;
    }

    public static void main(String[] args) {

    }
}
