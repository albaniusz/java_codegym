package com.codegym.task.task05.task0507;

/* 
Arithmetic mean

*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int counter = 0;
        float sum = 0;

        while (true) {
            int number = Integer.parseInt(reader.readLine());
            if (number == -1) {
                break;
            }

            sum += number;
            counter++;
        }

        System.out.println(sum / (float) counter);
    }
}

