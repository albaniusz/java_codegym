package com.codegym.task.task05.task0504;


/* 
The Three "Muscateers"

*/

public class Solution {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Pussy", 2, 2, 5);
        Cat cat2 = new Cat("Furry", 3, 3, 7);
        Cat cat3 = new Cat("Dong", 1, 2, 4);
    }

    public static class Cat {
        private String name;
        private int age;
        private int weight;
        private int strength;

        public Cat(String name, int age, int weight, int strength) {
            this.name = name;
            this.age = age;
            this.weight = weight;
            this.strength = strength;
        }
    }
}