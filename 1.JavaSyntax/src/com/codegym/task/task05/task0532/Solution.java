package com.codegym.task.task05.task0532;

import java.io.*;

/* 
Task about algorithms

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int maximum = 0;
        boolean firstTime = true;

        int n = Integer.parseInt(reader.readLine());

        for (int x = 0; x < n; x++) {
            int number = Integer.parseInt(reader.readLine());
            if (firstTime) {
                maximum = number;
                firstTime = false;
            } else {
                if (number > maximum) {
                    maximum = number;
                }
            }
        }

        System.out.println(maximum);
    }
}
