package com.codegym.task.task05.task0526;

/* 
Man and woman

*/

public class Solution {
    public static void main(String[] args) {
        Man man1 = new Man("Man1", 20, "address 1");
        Man man2 = new Man("Man2", 22, "address 2");
        System.out.println(man1);
        System.out.println(man2);

        Woman woman1 = new Woman("Woman1", 19, "address 3");
        Woman woman2 = new Woman("Woman2", 17, "address 4");
        System.out.println(woman1);
        System.out.println(woman2);
    }

    public static class Man {
        String name;
        int age;
        String address;

        public Man() {
            this.name = null;
            this.age = 0;
            this.address = null;
        }

        public Man(String name) {
            this.name = name;
            this.age = 0;
            this.address = null;
        }

        public Man(String name, int age) {
            this.name = name;
            this.age = age;
            this.address = null;
        }

        public Man(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }

        public String toString() {
            return name + " " + age + " " + address;
        }
    }

    public static class Woman {
        String name;
        int age;
        String address;

        public Woman() {
            this.name = null;
            this.age = 0;
            this.address = null;
        }

        public Woman(String name) {
            this.name = name;
            this.age = 0;
            this.address = null;
        }

        public Woman(String name, int age) {
            this.name = name;
            this.age = age;
            this.address = null;
        }

        public Woman(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }

        public String toString() {
            return name + " " + age + " " + address;
        }
    }
}
