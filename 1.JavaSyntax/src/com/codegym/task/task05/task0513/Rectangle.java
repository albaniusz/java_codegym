package com.codegym.task.task05.task0513;

/* 
Let's put together a rectangle

*/

public class Rectangle {
    private int top;
    private int left;
    private int width;
    private int height;

    public void initialize(int top) {
        initialize(top, 0);
    }

    public void initialize(int top, int left) {
        initialize(top, left, 0);
    }

    public void initialize(int top, int left, int width) {
        initialize(top, left, width, 0);
    }

    public void initialize(int top, int left, int width, int height) {
        this.top = top;
        this.left = left;
        this.width = width;
        this.height = height;
    }

    public static void main(String[] args) {

    }
}
