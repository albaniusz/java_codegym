package com.codegym.task.task05.task0509;

/* 
Create a Friend class

*/

public class Friend {
    private String name;
    private int age;
    private char sex;

    public void initialize(String name) {
        initialize(name, 0);
    }

    public void initialize(String name, int age) {
        initialize(name, age, 'M');
    }

    public void initialize(String name, int age, char sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public static void main(String[] args) {

    }
}
