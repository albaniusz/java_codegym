package com.codegym.task.task06.task0611;

/* 
StringHelper class

*/

public class StringHelper {
    public static String multiply(String s) {
        return StringHelper.multiply(s, 5);
    }

    public static String multiply(String s, int count) {
        String result = "";

        for (int x = 0; x < count; x++) {
            result += s;
        }

        return result;
    }

    public static void main(String[] args) {

    }
}
