package com.codegym.task.task06.task0621;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Cat relations

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Cat grandfather = new Cat(reader.readLine());
        Cat grandmother = new Cat(reader.readLine());
        Cat father = new Cat(reader.readLine(), grandfather, null);
        Cat mother = new Cat(reader.readLine(), null, grandmother);
        Cat son = new Cat(reader.readLine(), father, mother);
        Cat daughter = new Cat(reader.readLine(), father, mother);

        System.out.println(grandfather);
        System.out.println(grandmother);
        System.out.println(father);
        System.out.println(mother);
        System.out.println(son);
        System.out.println(daughter);
    }

    public static class Cat {
        private String name;
        private Cat father;
        private Cat mother;

        Cat(String name) {
            this.name = name;
        }

        Cat(String name, Cat father, Cat mother) {
            this.name = name;
            this.father = father;
            this.mother = mother;
        }

        @Override
        public String toString() {
            String output = "The cat's name is " + name;

            if (mother != null) {
                output += ", " + mother.name + " is the mother";
            } else {
                output += ", no mother";
            }

            if (father != null) {
                output += ", " + father.name + " is the father";
            } else {
                output += ", no father";
            }

            return output;
        }
    }

}
