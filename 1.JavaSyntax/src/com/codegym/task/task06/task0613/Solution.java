package com.codegym.task.task06.task0613;

/* 
Cat and statics

*/

public class Solution {
    public static void main(String[] args) {

        for (int x = 0; x < 10; x++) {
            Cat cat = new Cat();
        }

        System.out.println(Cat.catCount);
    }

    public static class Cat {
        static public int catCount;

        public Cat() {
            catCount++;
        }
    }
}
