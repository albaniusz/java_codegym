package com.codegym.task.task06.task0614;

import java.util.ArrayList;

/* 
Static cats

*/

public class Cat {
    public static ArrayList<Cat> cats = new ArrayList<>();

    public Cat() {
            }

    public static void main(String[] args) {

        for (int x = 0; x < 10; x++) {
            Cat.cats.add(new Cat());
        }

        printCats();
    }

    public static void printCats() {
        for (Cat element : Cat.cats) {
            System.out.println(element);
        }
    }
}
