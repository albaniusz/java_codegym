package com.codegym.task.task06.task0622;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Ascending numbers

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        ArrayList<Integer> list = new ArrayList<>();

        for (int x = 0; x < 5; x++) {
            list.add(Integer.parseInt(reader.readLine()));
        }

        boolean hasChange = true;
        while (hasChange) {
            hasChange = false;
            for (int x = 0; x < list.size() - 1; x++) {
                if (list.get(x) > list.get(x + 1)) {
                    int buffer = list.get(x);
                    list.set(x, list.get(x + 1));
                    list.set(x + 1, buffer);
                    hasChange = true;
                }
            }
        }

        for (Integer element : list) {
            System.out.println(element);
        }
    }
}
