package com.codegym.task.task06.task0605;


import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.io.*;

/* 
Controlling body weight

*/

public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
        double weight = Double.parseDouble(bis.readLine());
        double height = Double.parseDouble(bis.readLine());

        Body.calculateBMI(weight, height);
    }

    public static class Body {
        public static void calculateBMI(double weight, double height) {
            double index = weight / (height * height);
            String message = "";

            if (index < 18.5) {
                message = "Underweight: BMI < 18.5";
            } else if (index >= 18.5 && index <= 24.9) {
                message = "Normal: 18.5 <= BMI <= 24.9";
            } else if (index >= 25 && index <= 29.9) {
                message = "Overweight: 25 <= BMI <= 29.9";
            } else {
                message = "Obese: BMI >= 30";
            }

            System.out.println(message);
        }
    }
}
