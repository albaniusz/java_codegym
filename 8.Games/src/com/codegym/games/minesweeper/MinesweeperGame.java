package com.codegym.games.minesweeper;

import com.codegym.engine.cell.Color;
import com.codegym.engine.cell.Game;

public class MinesweeperGame extends Game {
    private int boardWidth = 50;
    private int boardHeight = 50;

    private void log(String messge) {
        System.out.println(messge);
    }

    @Override
    public void onTurn(int step) {
        super.onTurn(step);
        System.out.println("step!" + step);
    }

    @Override
    public void initialize() {
        log("initialize");

        log("showGrid: true");
        showGrid(true);

        log("setScreenSize: boardWidth, boardHeight");
        setScreenSize(boardWidth, boardHeight);

        for (int i = 0; i < boardWidth; i++) {
            for (int j = 0; j < boardHeight; j++) {
                setCellValueEx(i, j, Color.WHITE, "", Color.BLACK, 50);
            }
        }
setTurnTimer(500);


        setScore(0);
    }


    @Override
    public void onMouseLeftClick(int x, int y) {
        log("x, y" + x + " " + y);
        setCellValueEx(x, y, Color.WHITE, "!", Color.BLACK, 50);
    }
}
